<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delete_Model extends CI_Model {
	public function __construct(){
            $this->load->database();
    }

    public function delete($tablename='',$id='',$table_meta=[]){  
		
    	if($tablename) {
			
			if($table_meta){
				foreach($table_meta as $metaid => $table){
					$this->db->where($metaid, $id);
					$this->db->delete($table);
				}
				
			}
			     $this->db->where('id', $id);
		 return   $this->db->delete($tablename );
    	}else{
    		return false;
		}
		
        
    }
    
    public function deletewhere($tablename='',$where=array()){  
		
    	if($tablename) {
			$this->db->where($where);
		return	$this->db->delete($table);
			
    	}else{
    		return false;
		}
		
        
    }
}

/* End of file Insert_Model.php */
/* Location: ./application/models/Insert_Model.php */