<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insert_Model extends CI_Model {
	public function __construct(){
        $this->load->database();
    }

    public function insert($tablename='', $data_post=[]){ 
        if(!empty($data_post)){
            $this->db->insert($tablename, $data_post);
            return $this->db->insert_id();
        }else{
            return false;
        } 
    }

    public function commonInsertPostdata($tablename=''){
        $data_post = $this->input->post();
        return $this->db->insert($tablename, $data_post);
    }

    public function commonInsertItems($tablename='', $Items=''){
        if(!empty($Items)){
            return $this->db->insert($tablename, $Items);
            $insertId = $this->db->insert_id();
            return  $insertId;
        }
    }
}

/* End of file Insert_Model.php */
/* Location: ./application/models/Insert_Model.php */