<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Read_Model extends CI_Model {
	public function __construct(){
        $this->load->database();
	}
	
    public function dataread($tablename='',$feildarray=array(),$where='',$orderby='',$order='asc'){
		/*select part*/
       $string = '*';
		if(is_array($feildarray)){
		$string =	implode(',', $feildarray);
		}elseif(!empty($feildarray)){
           $string =  $feildarray;
        }
		$this->db->select($string);
		/*select part ends*/
       
       /*where part*/
		if(!empty($where)){			
			$this->db->where($where);
		}	  
       /*where part*/
       
       /*Conditon part*/
		if(!empty($orderby)){
			$this->db->order_by($orderby, $order);
		}
       /*Conditon part*/
		$query = $this->db->get($tablename);
		$row =  $query->result_array();
		if(!empty($row)){
			return $row;
		}else{
			return false;
		}
   }

    public function datareadIN($tablename='',$feildarray=array(),$wherein=array(),$find_in_set='',$orderby='',$order='asc',$limit='',$start='0'){
		$string = '*';
		if(is_array($feildarray)){
		$string =	implode(',', $feildarray);
		}
		$this->db->select($string);

		if(!empty($wherein)){	
			$this->db->where($wherein); //for and satement
		}	
		if(!empty($find_in_set)){			
         $this->db->like('category',$find_in_set, 'both');    	
		}
		 
		if(!empty($limit)){
			$this->db->limit($limit);
		}

		if(!empty($start)){
			$this->db->offset($start);
		}

		if(!empty($orderby)){
			$this->db->order_by($orderby, $order);
		}

		$query = $this->db->get($tablename);		
		$result =  $query->result_array();
		if(!empty($result)){
			return $result;
		}else{
			return false;
		}
	} 

    public function postMetaValue($post_id,$meta_key=''){
		$this->db->select('meta_value');
		$multipleWhere = ['post_id' => $post_id, 'meta_key' => $meta_key];
		$this->db->where($multipleWhere);
		$query = $this->db->get('gs_post_meta');
		return  $query->row('meta_value');
	}

	public function MenuMetaValue($menu_id,$meta_key=''){
		$this->db->select('meta_value');
		$multipleWhere = ['menu_id' => $menu_id, 'meta_key' => $meta_key];
		$this->db->where($multipleWhere);
		$query = $this->db->get('gs_menu_meta');
		return  $query->row('meta_value');
	}
}

/* End of file Read_Model.php */
/* Location: ./application/models/Read_Model.php */
