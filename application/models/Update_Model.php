<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_Model extends CI_Model {
	public function __construct(){
            $this->load->database();
    }

    public function UPDATE($id='',$table_name='',$data_post){   
        $this->db->where('id', $id);
      return  $this->db->update($table_name, $data_post);
      //  return $this->db->last_query();
    }

    public function UpadtePostMeta($postid='',$meta_key='',$metavalue){   
        $this->db->set('meta_value', $metavalue);  
        $this->db->where('post_id', $postid);
        $this->db->where('meta_key', $meta_key);
       return $this->db->update('gs_post_meta');
    }

    public function UpadteMenuMeta($menuid='',$meta_key='',$metavalue){   
        $this->db->set('meta_value', $metavalue);  
        $this->db->where('menu_id', $menuid);
        $this->db->where('meta_key', $meta_key);
       return $this->db->update('gs_menu_meta');
    }
}

/* End of file Insert_Model.php */
/* Location: ./application/models/Insert_Model.php */