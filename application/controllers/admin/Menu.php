<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Insert_Model');
        $this->load->model('Delete_Model');
        $this->load->model('Read_Model');
        $this->load->model('Update_Model');
        checkauth();
    } 
	public function index(){
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/menu');
        $this->load->view('admin/includes/footer.php');
	}

    public function menuitems(){
        $id = $this->input->post('id'); 
        $data2 =   $this->Read_Model->dataread('gs_menu','','post_id='.$id. ' and parent= 0','menu_order','asc');
       
        if($data2){    
            echo ChildLinks($data2);  //menu helper function     
        }else{
            echo "<h4 class='text-center'>No Links Foud..</h4>";
        }
       // pr($data2);
		
    }

	public function Add($value='')
	{
		/*Add category post request  */
	     $formdata = $this->input->post();
         /*set rules*/
		 $this->form_validation->set_rules('name', 'Menu Name', 'required');
         /*set rules ends*/

        if($this->form_validation->run() == True){ 
			
			$data_post = array('name'=>$formdata['name'],'post_type'=>'menu','is_active'=>'1');
            $data= $this->Insert_Model->insert('gs_posts',$data_post);
            $response =   response_message($data);
        	
        }else{
           $errors = $this->form_validation->error_array();
           $response =[
        		'status'=>"error",
        		'msg'  => 'Something Went Wrong Please Check Form Feilds..',
        		'inputs'=>$errors
        	];
        }

        echo json_encode($response);
    }
    
    public function AddMenuItem($value='')
	{
		/*Add category post request  */
	     $formdata = $this->input->post();
         /*set rules*/
		 $this->form_validation->set_rules('name', 'Menu Name', 'required');
         /*set rules ends*/

        if($this->form_validation->run() == True){ 
			
			$data_post = array('name'=>$formdata['name'],'parent'=>0,'menu_order'=>'','post_id'=>$formdata['get_menu'],'slug'=>$formdata['slug'],'is_visible'=>'1');
            $data= $this->Insert_Model->insert('gs_menu',$data_post);
            $response =   response_message($data);
        	
        }else{
           $errors = $this->form_validation->error_array();
           $response =[
        		'status'=>"error",
        		'msg'  => 'Something Went Wrong Please Check Form Feilds..',
        		'inputs'=>$errors
        	];
        }

        echo json_encode($response);
	}

    public function Delete($value='')
    {
        /*Add category post request  */
         $formdata = $this->input->post();
         if(isset($formdata['delete_menu'])){
            $metatable = array('post_id'=>'gs_menu'); //delete menu meta 
            $data= $this->Delete_Model->delete('gs_posts',$formdata['id'],$metatable);
            $response = response_message($data,' Deleted');
         }else{
            $metatable = array('menu_id'=>'gs_menu_meta'); //delete menu meta
            $data= $this->Delete_Model->delete('gs_menu',$formdata['id'],$metatable);
            $response =   response_message($data,' Deleted');
         }
           
            
         echo json_encode($response);
    }

    public function Edit($value='')
    {
        /*Add category post request  */
        $get_feilds = array('id','name','parent');
        $id = $this->input->post('id');
        $data['data'] =  $this->Read_Model->dataread('gs_categories',$get_feilds,'id = '.$id)[0];
        $data['allcat'] =  $this->Read_Model->dataread('gs_categories',$get_feilds);        
            if($data){
                $this->load->view('admin/ajax/cat-model',$data);
            }
    }

    public function update($value='')
	{  
        $formdata = $this->input->post();
        if(isset($formdata['visible_status'])){
            $status =1;
            if($formdata['is_visible'] == $status){
                $status =0;
            }
            $datapost = array('is_visible'=>$status);
            $data= $this->Update_Model->update($formdata['id'],'gs_menu',$datapost);
            $response =  response_message($data);
        }elseif(isset($formdata['change_name'])){
            $datapost = array('name'=>$formdata['change_name']);
            $data= $this->Update_Model->update($formdata['id'],'gs_posts',$datapost);
            $response =  response_message($data);

        }else{
            $this->form_validation->set_rules('name', 'Menu Name', 'required');
            if($this->form_validation->run() == True){ 
                $id = $formdata['edit'];
                unset($formdata['edit']); 
                $data= $this->Update_Model->update($id,'gs_menu',$formdata);
                $response =  response_message($data);
                
            }else{
            $errors = $this->form_validation->error_array();
            $response =[
                    'status'=>"error",
                    'msg'  => 'Something Went Wrong..',
                    'inputs'=>$errors
                ];
            }
        }
		

        echo json_encode($response);
    }
    
    public function savemenuorder()	{
		/*Add category post request  */
         $formdata = $this->input->post();
         $id = $this->input->post('id');
         save_menu_order(json_decode($formdata['menu_order'],true));
         $check = $this->Read_Model->dataread('gs_menu_meta','','menu_id = '.$formdata['id']);
         if($check){
            $data= $this->Update_Model->UpadteMenuMeta($id,'menu_order',$formdata['menu_order']);
            $response =   response_message($data);
         }else{

            $data_post = array('menu_id'=>$formdata['id'],'meta_key'=>'menu_order','meta_value'=>$formdata['menu_order']);
            $data= $this->Insert_Model->insert('gs_menu_meta',$data_post);
            $response =   response_message($data);

         }
         

        echo json_encode($response);
    }

    public function LoadUpdatemodel(){
        $id = $this->input->post('id');
        $data['link'] =   $this->Read_Model->dataread('gs_menu','','id='.$id)[0];
        $data['link_meta'] =   @$this->Read_Model->dataread('gs_menu_meta','','menu_id='.$id)[0];
        $this->load->view('admin/ajax/menu-link-model',$data);
    }

    public function LoadMenuOptions(){       
        $this->load->view('admin/ajax/menu-options');
    }
    

}