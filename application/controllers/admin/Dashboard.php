<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index(){
		checkauth();
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/dashboard');
        $this->load->view('admin/includes/footer.php');
	}
}

