<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

  public function __construct()
    {
        parent::__construct();
        $this->load->model('Insert_Model');
        $this->load->model('Delete_Model');
        $this->load->model('Read_Model');
        $this->load->model('Update_Model');
        checkauth();
    }

	public function index()
	{	
        $get_feilds = array('id','name','parent');
        $allcat['allcat'] =  $this->Read_Model->dataread('gs_categories',$get_feilds);
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/category',$allcat);
        $this->load->view('admin/includes/footer.php');
		$this->load->library('form_validation');
	}

	public function Add($value='')
	{
		/*Add category post request  */
	     $formdata = $this->input->post();
         /*set rules*/
		 $this->form_validation->set_rules('name', 'Category Name', 'required');
         /*set rules ends*/

        if($this->form_validation->run() == True){ 
             $data= $this->Insert_Model->insert('gs_categories',$formdata);

            if($data){
                $response =[
                    'status'=>"Success",
                    'msg'  => 'Category has successfully Added',
                    'inputs'=> ''
                ];
            }else{
                $response =[
                    'status'=>"Error",
                    'msg'  => 'query error',
                    'inputs'=> ''
                ];
            }
        	
        }else{
           $errors = $this->form_validation->error_array();
           $response =[
        		'status'=>"error",
        		'msg'  => 'Something Went Wrong..',
        		'inputs'=>$errors
        	];
        }

        echo json_encode($response);
	}

    public function Delete($value='')
    {
        /*Add category post request  */
         $formdata = $this->input->post();
         $data= $this->Delete_Model->delete('gs_categories',$formdata['id']);

            if($data){
                $response =[
                    'status'=>"Success",
                    'msg'  => 'Category has successfully Deleted',
                    'inputs'=> ''
                ];
            }else{
                $response =[
                    'status'=>"Error",
                    'msg'  => 'query error',
                    'inputs'=> ''
                ];
            }
            
        echo json_encode($response);
    }

    public function Edit($value='')
    {
        /*Add category post request  */
        $get_feilds = array('id','name','parent');
        $id = $this->input->post('id');
        $data['data'] =  $this->Read_Model->dataread('gs_categories',$get_feilds,'id = '.$id)[0];
        $data['allcat'] =  $this->Read_Model->dataread('gs_categories',$get_feilds,'id != '.$id);        
            if($data){
                $this->load->view('admin/ajax/cat-model',$data);
            }
    }

    public function update($value='')
	{
		/*Add category post request  */
	     $formdata = $this->input->post();
         /*set rules*/
		 $this->form_validation->set_rules('name', 'Category Name', 'required');
         /*set rules ends*/

        if($this->form_validation->run() == True){ 
            $id = $formdata['edit'];
            unset($formdata['edit']); 
             $data= $this->Update_Model->update($id,'gs_categories',$formdata);

            if($data){
                $response =[
                    'status'=>"Success",
                    'msg'  => 'Category has successfully updated',
                    'inputs'=> ''
                ];
            }else{
                $response =[
                    'status'=>"Error",
                    'msg'  => 'query error',
                    'inputs'=> ''
                ];
            }
        	
        }else{
           $errors = $this->form_validation->error_array();
           $response =[
        		'status'=>"error",
        		'msg'  => 'Something Went Wrong..',
        		'inputs'=>$errors
        	];
        }

        echo json_encode($response);
	}
    
    public function category_tree_options()
    {          
        $get_feilds = array('id','name','parent');
        $data = $this->Read_Model->dataread('gs_categories',$get_feilds,'parent=0');
        
      echo  json_encode(category_tree_json($data));
    }

       

}

/* End of file Category.php */
/* Location=> ./application/controllers/Category.php */
