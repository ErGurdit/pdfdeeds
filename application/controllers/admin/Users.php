<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

  public function __construct()
    {
        parent::__construct();
        $this->load->model('Insert_Model');
        $this->load->model('Delete_Model');
        $this->load->model('Read_Model');
        $this->load->model('Update_Model');
        checkauth();
    }

	public function index()
	{	
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/users');
        $this->load->view('admin/includes/footer.php');
	}

    public function Delete($value='')
    {
        /*Add category post request  */
         $formdata = $this->input->post();
         $data= $this->Delete_Model->delete('gs_users',$formdata['id']);
         $response =  response_message($data);
            
        echo json_encode($response);
    }

    public function update($value='')
	{  
        $formdata = $this->input->post();
        if(isset($formdata['visible_status'])){
            $status =1;
            if($formdata['is_active'] == $status){
                $status =0;
            }
            $datapost = array('is_active'=>$status);
            $data= $this->Update_Model->update($formdata['id'],'gs_users',$datapost);
            $response =  response_message($data);
        }
        echo json_encode($response);
    }
}

/* End of file Category.php */
/* Location=> ./application/controllers/Category.php */
