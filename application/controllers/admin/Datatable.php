<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datatable extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('Read_Model');
            checkauth();
    }

	public function categories(){
		
			$feilds = array( 'id','name', 'parent');
			$data= $this->Read_Model->dataread('gs_categories',$feilds);
			if(!empty($data)){
				$finaldata = array();
				$s =1;

				foreach ($data as $key => $row) {
					    $data[0]= $s;
		                $data[1]= $row['name'];
		                $data[2]=ucfirst($this->Read_Model->dataread('gs_categories','name',array('id'=>$row['parent']))[0]['name']);	
		                $data[3]="<button class='btn btn-sm btn-success edit' data-id='".$row['id']."'><i class='fa fa-edit'></i></button>
		                       <button class='btn btn-sm btn-danger delete' data-id='".$row['id']."'><i class='fa fa-trash'></i></button>";
					array_push($finaldata, $data);
					$s++;
				}

				 $arr1 = array("data"=>$finaldata);            
		        
			
		}else{
	            $arr1 = array("data"=>''); 
	    }
	     echo json_encode($arr1);
	}

	public function info(){		    
	    $type = $this->input->get('type');
		$feilds = array( 'id','name');
		$where = array('type'=>$type);
		$arr = array();
		$data= $this->Read_Model->dataread('gs_info',$feilds,$where);
		if(!empty($data)){
			$finaldata = array();
			$s =1;

			foreach ($data as $key => $row) {
				    $data[0]= $s;
	                $data[1]= $row['name'];
	                $data[2]="<button class='btn btn-sm btn-success edit'  feildsname='".$row['name']."' data-id='".$row['id']."'><i class='fa fa-edit'></i></button>
	                       <button class='btn btn-sm btn-danger delete' data-id='".$row['id']."'><i class='fa fa-trash'></i></button>";
				array_push($finaldata, $data);
				$s++;
			}

			 $arr1 = array("data"=>$finaldata);            
	         
		
		}else{
	        $arr1 = array("data"=>'');           
	             
	    }
	    echo json_encode($arr1);
	}

	public function Users(){
		
		$feilds = array( 'id','name', 'email','phone','is_active');
		$data= $this->Read_Model->dataread('gs_users',$feilds);
		if(!empty($data)){
			$finaldata = array();
			$s =1;

			foreach ($data as $key => $row) {
				$class='';
				if($row['is_active']==0 ){
					$class = 'disable';
				}
					$data[0]= $s;
					$data[1]= $row['name'];
					$data[2]=$row['email'];
					$data[3]=$row['phone'];
					$data[4]="
					<button type='button' class='btn btn-sm btn-default link_visible ".$class."' status='".$row['is_active']."' data-id='".$row['id']."'><i class='fa fa-eye'></i></button>
					<button class='btn btn-sm btn-danger delete' data-id='".$row['id']."'><i class='fa fa-trash'></i></button>";
				array_push($finaldata, $data);
				$s++;
			}

			 $arr1 = array("data"=>$finaldata);            
			 
		
		}else{
				$arr1 = array("data"=>'');             
				
		}
		 echo json_encode($arr1);
	}

	public function files(){
		
		$feilds = array( 'id','name', 'category','lang');
		$data= $this->Read_Model->dataread('gs_posts',$feilds,'post_type="books"');
		if(!empty($data)){
			$finaldata = array();
			$s =1;
			
			foreach ($data as $key => $row) {
					$catenames = '';
					$cats = explode(',', $row['category']);		
					foreach ($cats as  $value) {
						$where = array('id'=>$value);
						$name = $this->Read_Model->dataread('gs_categories','name',$where)[0]['name'].', ';
						if(!empty($name)){
								$catenames .= $name ;
						}
					
					}	
				$feilds1 = array( 'meta_vaue');
					$data[0]= $s;
					$data[1]= $row['name'];
					$data[2]=ucfirst($this->Read_Model->postMetaValue($row['id'],'author'));
					$data[3]=ucfirst($this->Read_Model->postMetaValue($row['id'],'publisher'));
					$data[4]=ucfirst($row['lang']);
					$data[5] =rtrim($catenames, ",");
					$data[6]="<button class='btn btn-sm btn-success edit' data-id='".$row['id']."'><i class='fa fa-edit'></i></button>
						   <button class='btn btn-sm btn-danger delete' data-id='".$row['id']."'><i class='fa fa-trash'></i></button>";
				array_push($finaldata, $data);
				$s++;
			}

			 $arr1 = array("data"=>$finaldata);            
			 
		
		}else{
				$arr1 = array("data"=>'');             
				
		}
		echo json_encode($arr1);
	}

	public function menu(){
		
		$feilds = array( 'id','name');
		$data= $this->Read_Model->dataread('gs_posts',$feilds,'post_type="menu"');
		if(!empty($data)){
			$finaldata = array();
			$s =1;
			
			foreach ($data as $key => $row) {
				
				$feilds1 = array( 'meta_vaue');
					$data[0]= $s;
					$data[1]= $row['name'];
					$data[2]="<button class='btn btn-sm btn-success edit' menuname='".$row['name']."' data-id='".$row['id']."'><i class='fa fa-edit'></i></button>
						     <button class='btn btn-sm btn-danger delete' data-id='".$row['id']."'><i class='fa fa-trash'></i></button>";
				array_push($finaldata, $data);
				$s++;
			}

			 $arr1 = array("data"=>$finaldata);            
			 
		
		}else{
			   $arr1 = array("data"=>'');            
				
		}
		echo json_encode($arr1);
	}

}

/* End of file Datatable_post.php */
/* Location: ./application/controllers/Datatable_post.php */