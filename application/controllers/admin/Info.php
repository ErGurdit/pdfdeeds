<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {

  public function __construct()
    {
        parent::__construct();
        $this->load->model('Insert_Model');
        $this->load->model('Delete_Model');
        $this->load->model('Read_Model');
        $this->load->model('Update_Model');
        $this->load->library('form_validation');
        checkauth();
    }

	public function index()
	{	
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/info');
        $this->load->view('admin/includes/footer.php');
		
	}

	public function Add($value='')
	{
		/*Add category post request  */
	     $formdata = $this->input->post();
         /*set rules*/
		 $this->form_validation->set_rules('name', 'Please enter  Name', 'required');
         /*set rules ends*/

        if($this->form_validation->run() == True){ 
            $where = array('name'=>$formdata['name'],'type'=>$formdata['type']);
            $check = $this->Read_Model->dataread('gs_info','',$where);
            if(empty($check)){
                $data= $this->Insert_Model->insert('gs_info',$formdata);
                 if($data){
                    $response =[
                        'status'=>"success",
                        'msg'  => $formdata['type'].' has successfully Added',
                        'inputs'=> ''
                    ];
                }else{
                    $response =[
                        'status'=>"error",
                        'msg'  => 'query error',
                        'inputs'=> ''
                    ];
                }
            }else{
                 $response =[
                    'status'=>"error",
                    'msg'  => $formdata['type'].' with same name alreay Exist',
                    'inputs'=> ''
                ];
            }

            
        	
        }else{
           $errors = $this->form_validation->error_array();
           $response =[
        		'status'=>"error",
        		'msg'  => 'Something Went Wrong..',
        		'inputs'=>$errors
        	];
        }

        echo json_encode($response);
	}

    public function Delete($value='')
    {
        /*Add category post request  */
         $formdata = $this->input->post();
         $data= $this->Delete_Model->delete('gs_info',$formdata['id']);

            if($data){
                $response =[
                    'status'=>"Success",
                    'msg'  => 'Category has successfully Deleted',
                    'inputs'=> ''
                ];
            }else{
                $response =[
                    'status'=>"Error",
                    'msg'  => 'query error',
                    'inputs'=> ''
                ];
            }
            
        echo json_encode($response);
    }

    public function update($value='')
	{
		/*Add category post request  */
	     $formdata = $this->input->post();
         /*set rules*/
		 $this->form_validation->set_rules('name', 'Category Name', 'required');
         /*set rules ends*/

        if($this->form_validation->run() == True){ 
            $id = $formdata['id'];
            unset($formdata['id']); 
             $where = array('id'=>$id);
             $check = $this->Read_Model->dataread('gs_info','',$where)[0];
             if($check['name'] != $formdata['name']){
                $data= $this->Update_Model->update($id,'gs_info',$formdata);

                if($data){
                    $response =[
                        'status'=>"success",
                        'msg'  => 'Data has successfully updated',
                        'inputs'=> ''
                    ];
                }else{
                    $response =[
                        'status'=>"error",
                        'msg'  => 'query error',
                        'inputs'=> ''
                    ];
                }
             }else{
                $response =[
                        'status'=>"error",
                        'msg'  => $check['type'].' with same value already Exist',
                        'inputs'=> ''
                    ];
             }
             
        	
        }else{
           $errors = $this->form_validation->error_array();
           $response =[
        		'status'=>"error",
        		'msg'  => 'Something Went Wrong..',
        		'inputs'=>$errors
        	];
        }

        echo json_encode($response);
	}
    
    public function options(){         
        $type = $this->input->post('type');
        $feilds = array( 'id','name');
        $where = array('type'=>$type);        
        $data= $this->Read_Model->dataread('gs_info',$feilds,$where);
        if(!empty($data)){
            foreach ($data as $key => $row) {
               echo "<option value='".$row['id']."'>".$row['name']."</option>";
            }
        }else{
               echo "<option value=''>Others</option>";         
                 
        }
    }
}

/* End of file Category.php */
/* Location=> ./application/controllers/Category.php */
