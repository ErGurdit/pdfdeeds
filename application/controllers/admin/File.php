<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller {

  public function __construct()
  { 
      parent::__construct(); 
      $this->load->helper('url'); 
      $this->load->model('Insert_Model');
      $this->load->model('Delete_Model');
      $this->load->model('Read_Model');
      $this->load->model('Update_Model');
      checkauth();
   }
  

  public function index()
  {
    $awhere = array('type'=>'author');
    $Pwhere = array('type'=>'publisher');
    $data['authors']= $this->Read_Model->dataread('gs_info','',$awhere);
    $data['publishers']= $this->Read_Model->dataread('gs_info','',$Pwhere);
		$this->load->view('admin/includes/header.php');
		$this->load->view('admin/file',$data);
    $this->load->view('admin/includes/footer.php');
	}

	
  public function filedata($value='')
  { 
    
    $formdata = $this->input->post();
    /*set rules*/
    $this->form_validation->set_rules('name', 'Book Name', 'required');
    $this->form_validation->set_rules('author', 'Author Name', 'required');
    $this->form_validation->set_rules('publisher', 'publisher Name', 'required');
    $allowed_files =  array('pdf'); 
    /*set rules ends*/
    $errors ='';
   if($this->form_validation->run() == True && check_file($_FILES,$allowed_files)){ 
    // start post insert
    $category = $formdata['category'];
    $item_feilds = array(
      'name' => $formdata['name'],
      'title'=>$formdata['name'],
      'category' =>"$category",
      'post_type' => 'books',
      'lang' => $formdata['lang'],
      'is_active' => '1',
      'post_type' => 'books',
      'description' => $formdata['description']
    );

     $this->Insert_Model->commonInsertItems('gs_posts',$item_feilds);
    //  ends with post insert
    // start with post meta insert
     $insertId = $this->db->insert_id();
     /*upload book cover*/
       $featured_image = '';
      if(!empty($formdata['bookcover'])){
          $data = $_POST['bookcover'];
          list($type, $data) = explode(';', $data);
          list(, $data)      = explode(',', $data);
          $data = base64_decode($data);
          $featured_image = time().'.png';
          file_put_contents('bookscover/'.$featured_image, $data);
      }
     /*ends with upload book cover*/

    $meta_items = array(
      'author' => $formdata['author'],
      'publisher' => $formdata['publisher'],
      'cover_image'=> $featured_image,
    ); 
    foreach($meta_items as $key => $value){
      if(!empty($value)){ /*check if value is not empty*/
         $Items = [
           'post_id' => $insertId,
           'meta_key' => $key,
           'meta_value' =>$value

        ];
        $this->Insert_Model->commonInsertItems('gs_post_meta',$Items); 
      }      
    }
      
    // ends with post meta insert
    $count = count($_FILES['files']['name']);
    $totalFiles=array();
    for($i=0;$i<$count;$i++){
  
      if(!empty($_FILES['files']['name'][$i])){
  
        $_FILES['file']['name'] = $_FILES['files']['name'][$i];
        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
        $_FILES['file']['size'] = $_FILES['files']['size'][$i];
        // configration
        $config['upload_path'] = 'gs_uploads/'; 
        $config['allowed_types'] = 'pdf|jpeg|png|gif';
        $config['max_size'] = '5000';
        $config['file_name'] =  time();
        // configration
        $this->load->library('upload',$config); 
        if($this->upload->do_upload('file')){
          $uploadData = $this->upload->data();
          $filename = $uploadData['file_name'];
          array_push($totalFiles,$filename);
          $Items = [
            'post_id' => $insertId,
            'meta_key' => 'file',
            'meta_value' => $filename
    
          ];
       $data =   $this->Insert_Model->commonInsertItems('gs_post_meta',$Items);
        }
      }      
 
    }  // end of foreach loop

    if($data){
        $response =[
          'status'=>"success",
          'msg'  => count($totalFiles).' Files has successfully Uploaded'
      ];
    }else{
      $response =[
          'status'=>"error",
          'msg'  => 'query error',
      ];
    }
 
     
   }else{
      $errors = $this->form_validation->error_array();
      $msg = "please upload only PDF";
      if(check_file($_FILES,$allowed_files)){
        $msg =  'Something Went Wrong..';
      }
      $response =[
       'status'=>"error",
       'msg'  => $msg,
       'inputs'=>$errors
     ];
   }
   echo json_encode($response);
 
  }
        

  public function Delete($value='')
  {
      /*Add category post request  */
       $formdata = $this->input->post();
       $metatable = array('post_id'=>'gs_post_meta');
           $data= $this->Delete_Model->delete('gs_posts',$formdata['id'],$metatable);

          if($data){
              $response =[
                  'status'=>"Success",
                  'msg'  => 'Book has Deleted successfully',
                  'inputs'=> ''
              ];
          }else{
              $response =[
                  'status'=>"Error",
                  'msg'  => 'query error',
                  'inputs'=> ''
              ];
          }
          
      echo json_encode($response);
  }

  public function Edit($value='')
  {
      /*Add category post request  */
      $get_feilds = array('id','name','parent');
      $id = $this->input->post('id');
      $awhere = array('type'=>'author');
      $Pwhere = array('type'=>'publisher');
      $data['authors']= $this->Read_Model->dataread('gs_info','',$awhere);
      $data['publishers']= $this->Read_Model->dataread('gs_info','',$Pwhere);
      $data['data'] =      $this->Read_Model->dataread('gs_posts','','id = '.$id)[0];
      $data['allcat'] =    $this->Read_Model->dataread('gs_categories',$get_feilds);       
      $data['cover_image'] = $this->Read_Model->postMetaValue($id,'cover_image');  
      if($data){
          $this->load->view('admin/ajax/file-model',$data);
      }
  }

  public function update($value='')
  {
  /*Add category post request  */
     $formdata = $this->input->post();
      /*set rules*/
     $this->form_validation->set_rules('name', 'Book Name', 'required');
     $this->form_validation->set_rules('author', 'author Name', 'required');
     $this->form_validation->set_rules('publisher', 'publisher Name', 'required');
       /*set rules ends*/

      if($this->form_validation->run() == True){ 
           $id = $formdata['edit'];
           unset($formdata['edit']); //remove row id from post array
           /*=================Cover image section=================*/
           $featured_image = '';
           if($formdata['cover_image'] != $formdata['old_cover_image']){
              if(!empty($formdata['cover_image'])){
                  $data = $_POST['cover_image'];
                  list($type, $data) = explode(';', $data);
                  list(, $data)      = explode(',', $data);
                  $data = base64_decode($data);
                  $featured_image = time().'.png';
                  file_put_contents('bookscover/'.$featured_image, $data);
              }
              $path = 'bookscover/'.$formdata['old_cover_image'];
              if(file_exists($path)){
                @unlink($path); //delete the old image
              }
             
           }else{
            $featured_image = $formdata['old_cover_image'];
           }
          unset($formdata['old_cover_image']); // remove from post data
           /*=================Cover image section=================*/
           
           /*=================Insert Post meta=================*/          
           
           $post_meta = array('author','publisher','cover_image');
           foreach($post_meta  as $value){
              $metavalue =  $this->input->post($value);
              if( $value =='cover_image'){
                 $metavalue = $featured_image; //give the name of uploaded image
              }
              $result = $this->Read_Model->postMetaValue($id,$value);
              if(!empty($result)){
                $this->Update_Model->UpadtePostMeta($id,$value,$metavalue);
              }else{
                    $Items = [
                      'post_id' => $id,
                      'meta_key' => $value,
                      'meta_value' => $metavalue            
                    ];
                    if(!empty($metavalue)){
                      $this->Insert_Model->commonInsertItems('gs_post_meta',$Items);
                    }
                
              }           
              unset($formdata[$value]); //remvove value from post 
           }
           /*=================Insert Post meta=================*/

           $data= $this->Update_Model->update($id,'gs_posts',$formdata);
            if($data){
                $response =[
                    'status'=>"Success",
                    'msg'  => 'Book Info has updated',
                    'inputs'=> ''
                ];
            }else{
                $response =[
                    'status'=>"Error",
                    'msg'  => 'query error',
                    'inputs'=> ''
                ];
            }
        
      }else{
         $errors = $this->form_validation->error_array();
         $response =[
            'status'=>"error",
            'msg'  => 'Something Went Wrong..',
            'inputs'=>$errors
          ];
      }

    echo json_encode($response);
}
  
  

}

/* End of file File.php */
/* Location: ./application/controllers/File.php */
