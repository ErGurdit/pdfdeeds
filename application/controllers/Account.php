<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

 
    public function __construct()
    {
    parent::__construct();
    $this->load->model('Insert_Model');
    $this->load->model('Delete_Model');
    $this->load->model('Read_Model');
    $this->load->model('Update_Model');
    } 

	public function index()
	{	       
        $view = 'account';
        if(isset($_SESSION['logged_in'])){
            $view = 'message';
        }
        $this->load->view('includes/header.php');
		$this->load->view($view);
        $this->load->view('includes/footer.php');
	}
    


    public function login()
    {
         /*Add category post request  */
         $formdata = $this->input->post();
         /*set rules*/
         $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
         $this->form_validation->set_rules('password', 'Password', 'required');
          /*set rules ends*/
 
         if($this->form_validation->run() == True){ 
             $where = array(
             'email' => $this->input->post('email'),
             'password' => md5($this->input->post('password'))
             );
             
             $data= $this->Read_Model->dataread('gs_users','',$where)[0];
 
             if($data){
                $newdata = array( 
                    'name'  => $data['name'], 
                    'email'     => $data['email'],
                    'check' => $data['role'],
                    'logged_in' => TRUE
                 );  
                 
                 $this->session->set_userdata($newdata);
                 $response =[
                     'status'=>"Success",
                     'msg'  => 'Welcome '.$data['name'],
                     'inputs'=> '',
                     'redirect'=>base_url('admin/dashboard')
                 ];    
                  //$this->send_verify_mail($data);
             }else{
                 $response =[
                     'status'=>"Error",
                     'msg'  => 'Please check the login Details',
                     'inputs'=> ''
                 ];
             }
             
         }else{
             $errors = $this->form_validation->error_array();
             $response =[
                 'status'=>"error",
                 'msg'  => 'Something Went Wrong..',
                 'inputs'=>$errors
             ];
         }
         echo json_encode($response);
    }


    public function Add($value='')
    {
        /*Add category post request  */
        $formdata = $this->input->post();
        /*set rules*/
        $this->form_validation->set_rules('name', 'User Name', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[gs_users.email]');
        $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');
        /*set rules ends*/

        if($this->form_validation->run() == True){ 
            $user = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'password' => md5($this->input->post('password')),
            'verify' => random(),
            'role' => '2',
            'created_at'=>current_date_time()            
            );
            
                $data= $this->Insert_Model->insert('gs_users',$user);

            if($data){
                echo $data;
                /*=================== fetching user data mail to the user ===================*/
                $feilds = array('id','email','verify','name');
                $where = array('id'=>$data);
                $userdata = $this->Read_Model->dataread('gs_users',$feilds,$where);
                //$check = $this->send_verify_mail($userdata);
                /*=================== fetching user data mail to the user ===================*/
                if($check){
                     $response =[
                        'status'=>"Success",
                        'msg'  => 'Account sccessfuly created please check your email verify your account',
                        'inputs'=> ''
                    ];
                }else{
                    $response =[
                        'status'=>"Success",
                        'msg'  => 'Account sccessfuly created But email in not send contact us on contact us page',
                        'inputs'=> ''
                    ];
                }
               
            }else{
                $response =[
                    'status'=>"Error",
                    'msg'  => 'query error',
                    'inputs'=> ''
                ];
            }
            
        }else{
            $errors = $this->form_validation->error_array();
            $response =[
                'status'=>"error",
                'msg'  => 'Something Went Wrong..',
                'inputs'=>$errors
            ];
        }

            echo json_encode($response);
	}

    public function Delete($value='')
    {
        /*Add category post request  */
         $formdata = $this->input->post();
             $data= $this->Delete_Model->delete('gs_categories',$formdata['id']);

            if($data){
                $response =[
                    'status'=>"Success",
                    'msg'  => 'Category has successfully Deleted',
                    'inputs'=> ''
                ];
            }else{
                $response =[
                    'status'=>"Error",
                    'msg'  => 'query error',
                    'inputs'=> ''
                ];
            }
            
        echo json_encode($response);
    }

    public function Edit($value='')
    {
        /*Add category post request  */
        $get_feilds = array('id','name','parent');
        $id = $this->input->post('id');
        $data['data'] =  $this->Read_Model->dataread('gs_categories',$get_feilds,'id = '.$id)[0];
        $data['allcat'] =  $this->Read_Model->dataread('gs_categories',$get_feilds,'id != '.$id);        
            if($data){
                $this->load->view('admin/ajax/cat-model',$data);
            }
    }

    public function update($value='')
	{
		/*Add category post request  */
	     $formdata = $this->input->post();
         /*set rules*/
		 $this->form_validation->set_rules('name', 'Category Name', 'required');
         /*set rules ends*/

        if($this->form_validation->run() == True){ 
            $id = $formdata['edit'];
            unset($formdata['edit']); 
             $data= $this->Update_Model->update($id,'gs_categories',$formdata);

            if($data){
                $response =[
                    'status'=>"Success",
                    'msg'  => 'Category has successfully updated',
                    'inputs'=> ''
                ];
            }else{
                $response =[
                    'status'=>"Error",
                    'msg'  => 'query error',
                    'inputs'=> ''
                ];
            }
        	
        }else{
           $errors = $this->form_validation->error_array();
           $response =[
        		'status'=>"error",
        		'msg'  => 'Something Went Wrong..',
        		'inputs'=>$errors
        	];
        }

        echo json_encode($response);
	}
    
    public function verify($code=0){    
        $code =$this->input->get('code');
        $email =$this->input->get('email');
        $where =array('email'=>$email);
        $data = $this->Read_Model->dataread('gs_users','',$where)[0];
        if(!empty($data['verify']) != $code || $code=='' ){
            $data['msg'] = "Your account is alredy verified";
        }elseif($data['verify'] == $code){
            $up = array('verify'=>'','is_active'=>1);
            $data= $this->Update_Model->update($data['id'],'gs_users',$up);
            $data['msg'] = "<a href='".base_url()."'>Go Back Home</a>";
        
        }else{
            $data['msg'] = "sorry Your code is not valid";
        }

        $this->load->view('includes/header.php');
        $this->load->view('message',$data);
        $this->load->view('includes/footer.php');
    }

    
    public function logout(){
        $sessions = array('name','email','logged_in','check');  
        $this->session->unset_userdata($sessions);
        redirect(base_url()); 
    }

    public function send_verify_mail($data=array()){
        $this->load->config('email');
        $this->load->library('email');
        if($data){
            $from_email = "email@example.com";
            $to_email = $data['email'];
            $data['data'] =$data;
            $html = $this->load->view('email-template',$data,True);
            //Load email library
            $this->load->library('email');
            $this->email->from($from_email, 'Identification');
            $this->email->to($to_email);
            $this->email->subject('Verify Account From books lovers');
            $this->email->message($html);
            //Send mail
            if($this->email->send()){
               return true;
            }else{
               show_error($this->email->print_debugger());
            } 
         }else{
            return false;
         }
    }

}