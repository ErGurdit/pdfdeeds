<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {

  public function _construct()
    {
        parent::__construct();
        $this->load->model('Insert_Model');
        $this->load->model('Delete_Model');
        $this->load->model('Read_Model');
        $this->load->model('Update_Model');
        
    }

	public function index($val='')
	{	       
        $limit = 1;
        $offset = 1;
        $data['offset'] =$offset;
        $data['limit'] =1;
        $data['books']= Books('' ,$val,$limit); //helper function
        $data['total'] = count(Books('' ,$val));
        $data['urlid'] = $val;
    		$this->load->view('includes/header.php');
    		$this->load->view('books',$data);
        $this->load->view('includes/footer.php');
    }

    public function loadmore(){
        $limit =  1;
        $offset = $this->input->post('offset'); 
        $val = $this->input->post('urlid');  
        $books['books']= Books('' ,$val,$limit,$offset);        
        $this->load->view('ajax/booksloadmore',$books);
        
       
    }

}
