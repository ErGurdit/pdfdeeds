<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function _construct()
    {
        parent::__construct();
        $this->load->model('Insert_Model');
        $this->load->model('Delete_Model');
        $this->load->model('Read_Model');
        $this->load->model('Update_Model');
    }

	public function index()
	{	       
		$this->load->view('includes/header.php');
		$this->load->view('home');
        $this->load->view('includes/footer.php');
  }



}
