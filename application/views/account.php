<!-- login-register -->
<section id="login-register">
    <div class="container">
        <div class="row">
            <!-- login form 1 -->
            <div class="col-sm-6">
                <form id="loginform" method="post">
                    <div id="login-form">
                        <h3 class="log-title">LOGIN</h3>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="PASSWORD">
                            <div class="help-block with-errors"></div>
                        </div>
                        <!-- log-line -->
                        <div class="log-line">
                            <!-- <div class="pull-left">
                            <div class="checkbox checkbox-primary space-bottom">
                                <label class="hide"><input type="checkbox"></label>
                                <input id="checkbox1" type="checkbox">
                                <label for="checkbox1"><span><strong>Remember Me</strong></span></label>
                            </div>
                        </div> -->
                            <div class="pull-right">
                                <button type="submit" class="btn btn-md btn-primary-filled btn-log btn-rounded">Log
                                    In</button>
                                <div id="msgSubmit" class="h3 text-center hidden"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div><!-- / log-line -->
                        <a href="#x" class="forgot-password">Forgot your Password?</a>
                    </div>
                </form>
            </div><!-- / col-sm-6 -->
            <!-- / login form 1 -->

            <!-- register form 1 -->
            <div class="col-sm-6">
                <form id="registerform" action='' method='post'>
                    <div id="register-form">
                        <h3 class="log-title">REGISTER</h3>
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Name">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="EMAIL"
                                data-error="*Please fill out this field">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" name="phone" placeholder="Phone Number">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="PASSWORD"
                                data-error="*Please fill out this field">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="cpassword" placeholder="CONFIRM PASSWORD">
                            <div class="help-block with-errors"></div>
                        </div>
                        <!-- log-line -->
                        <div class="log-line reg-form-1 no-margin">
                            <div class="pull-right">
                                <button type="submit" id="reg-submit"
                                    class="btn btn-md btn-primary-filled btn-log btn-rounded">Register</button>
                                <div id="register-msgSubmit" class="h3 text-center hidden"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div><!-- / log-line -->
                    </div>
                </form>
            </div><!-- / col-sm-6 -->
            <!-- / register form 1 -->
        </div><!-- / row -->
        <!-- / form 1 -->
    </div><!-- / container -->
</section>
<!-- / login-register -->

<!-- / content -->