<?php

 if(!empty($books)){
    $count = 0;
    $lastid = end($books)['id'];
    foreach ($books as $book) {
  $cover_image = base_url('assets/images/m-product1.jpg');
    if(!empty($book['cover_image'])){
        $path = base_url('bookscover/'.$book['cover_image']);
            $cover_image = $path;                             
                        
     }
?>                              
    <div class="col-xs-3 product m-product" data-groups='["bedroom"]'>
        <div class="img-bg-color primary">
            <a href="single-product2.html" class="product-link"></a>
            <!-- / product-link -->
            <img style="width:265px; height: 350px;" src="<?= $cover_image;?>" alt="">
            <!-- / product-image -->

            <!-- product-hover-tools -->
            <div class="product-hover-tools">
                <a  target="_blank" href="single-product2.html" class="view-btn" data-toggle="tooltip" title="View Product">
                    <i class="lnr lnr-eye"></i>
                </a>
              <a target="_blank" href="<?= filedownloadpath($book['file']);?>" class="cart-btn" data-toggle="tooltip" title="Dwonload File">
                    <i class="fa fa-file"></i>
                </a>
            </div><!-- / product-hover-tools -->

            <!-- product-details -->
            <div class="product-details">
                <h5 class="product-title"><?= $book['name'];?></h5>
                <p class="product-category"><?= $book['category'];?></p>
            </div><!-- / product-details -->
        </div><!-- / img-bg-color -->
    </div>
                        <!-- / product -->               
    <?php $count++; }
  /*  echo('<input type="hidden" name="lastid" id="lastid" value="'.$lastid.'"/>');*/
    }

    ?>
  