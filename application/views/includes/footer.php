<!-- scroll to top -->
<a href="#top" class="scroll-to-top page-scroll is-hidden" data-nav-status="toggle"><i class="fa fa-angle-up"></i></a>
<!-- / scroll to top -->
<!-- footer -->
<footer class="light-footer">
    <div class="widget-area">
        <div class="container">
            <div class="row">

                <div class="col-md-4 widget">
                    <div class="about-widget">
                        <div class="widget-title-image">
                            <img src="<?= base_url();?>assets/images/logo2.png" alt="">
                        </div>
                        <p>Vivamus consequat lacus quam, nec egestas quam egestas sit amet. Suspendisse et risus gravida tellus aliquam ullamcorper. Pellentesque elit dolor, ornare ut lorem nec, convallis nibh accumsan lacus morbi leo lipsum.</p>
                    </div><!-- / about-widget -->
                </div><!-- / widget -->
                <!-- / first widget -->

                <div class="col-md-2 widget">
                    <div class="widget-title">
                        <h4>Top School</h4>
                    </div>
                     <?php
                    $parntclass= array('link-widget');
                    $childclass= array('info');
                    
                   echo  MenuHtml('top schools',$parntclass,$childclass);?>                    
                </div><!-- / widget -->
                <!-- / second widget -->

                <div class="col-md-2 widget">
                    <div class="widget-title">
                        <h4>SUPPORT</h4>
                    </div>
                    <div class="link-widget">
                        <div class="info">
                            <a href="#x">Terms & Conditions</a>
                        </div>
                        <div class="info">
                            <a href="#x">Shipping & Return</a>
                        </div>
                        <div class="info">
                            <a href="faq.html">F.A.Q</a>
                        </div>
                        <div class="info">
                            <a href="contact.html">Contact</a>
                        </div>
                    </div>
                </div><!-- / widget -->
                <!-- / third widget -->

                <div class="col-md-4 widget">
                    <div class="widget-title">
                        <h4>CONTACT</h4>
                    </div>
                    <div class="contact-widget">
                        <div class="info">
                            <p><i class="lnr lnr-map-marker"></i><span>Miami, S Miami Ave, SW 20th, Store No.1</span></p>
                        </div>
                        <div class="info">
                            <a href="tel:+0123456789"><i class="lnr lnr-phone-handset"></i><span>+0123 456 789</span></a>
                        </div>
                        <div class="info">
                            <a href="mailto:hello@yoursite.com"><i class="lnr lnr-envelope"></i><span>office@yoursite.com</span></a>
                        </div>
                        <div class="info">
                            <i class="lnr lnr-thumbs-up"></i>
                            <span class="social text-left">
                                <a class="no-margin" href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </span>
                        </div>
                    </div><!-- / contact-widget -->
                </div><!-- / widget -->
                <!-- / fourth widget -->

            </div><!-- / row -->
        </div><!-- / container -->
    </div><!-- / widget-area -->
    <div class="footer-info">
        <div class="container">
            <div class="pull-left copyright">
                <p><strong>© MS - MINIMAL SHOP THEME</strong></p>
            </div>
            <span class="pull-right">
                <img src="<?= base_url();?>assets/images/visa.png" alt="">
                <img src="<?= base_url();?>assets/images/mastercard.png" alt="">
                <img src="<?= base_url();?>assets/images/discover.png" alt="">
                <img src="<?= base_url();?>assets/images/paypal.png" alt="">
            </span>
        </div><!-- / container -->
    </div><!-- / footer-info -->
</footer>
<!-- / footer -->

<!-- javascript -->
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/admin_assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/js/common.js"></script>
<!-- sticky nav -->
<script src="<?= base_url()?>assets/js/jquery.easing.min.js"></script>
<script src="<?= base_url()?>assets/js/scrolling-nav-sticky.js"></script>
<!-- / sticky nav -->

<!-- shop -->
<script src="<?= base_url()?>assets/js/custom.js"></script>
<script src="<?= base_url()?>assets/js/jquery.shuffle.min.js"></script>
<!-- / shop -->

<!-- brands carousel -->
<script src="<?= base_url()?>assets/js/owl.carousel.min.js"></script>
<script>
    $(document).ready(function() {
        $("#brands-carousel").owlCarousel({
            autoPlay: 5000, //set autoPlay to 5 seconds.
            items: 5,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3]
        });

    });

    var baseurl = "<?= base_url();?>";

</script>
<!-- / brands carousel -->

<!-- preloader -->
<script src="<?= base_url()?>assets/js/preloader.js"></script>
<!-- / preloader -->

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

</script>

<!-- hide nav -->
<script src="<?= base_url()?>assets/js/hide-nav.js"></script>
<!-- / hide nav -->
<script src="<?= base_url()?>assets/js/functions.js"></script>
<!-- / javascript -->
<script type="text/javascript">
   /* $('.main-menu').append($('.allcategorydrop').html());
    $('.allcategorydrop').remove();*/
    //$('.appendmenu').toggleClass('dropdown-menu animated zoomIn fast')
</script>
</body>


<!-- Mirrored from kingstudio.ro/demos/ms/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 Mar 2020 15:26:41 GMT -->

</html>
