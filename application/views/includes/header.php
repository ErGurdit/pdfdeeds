<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Minimal Shop Theme">
    <meta name="keywords" content="responsive, retina ready, html5, css3, shop, market, onli store, bootstrap theme" />
    <meta name="author" content="KingStudio.ro">

    <!-- favicon -->
    <link rel="icon" href="<?= base_url()?>assets/images/favicon.png">
    <!-- page title -->
    <title>MS - Minimal Shop Theme</title>
    <!-- bootstrap css -->
    <link href="<?= base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- css -->
    <link href="<?= base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/animate.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/owl.theme.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/admin_assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lily+Script+One" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='<?= base_url()?>assets/fonts/FontAwesome.otf' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= base_url()?>assets/css/linear-icons.css">
    <link href="<?= base_url()?>assets/custom.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>



<body>

    <!-- preloader -->
    <div id="preloader">
        <div class="spinner spinner-round"></div>
    </div>
    <!-- / preloader -->

    <div id="top"></div>

    <!-- header -->
    <header>

        <!-- nav -->
        <nav class="navbar navbar-default nav-sec navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= base_url()?>assetsindex.html"><img src="<?= base_url();?>assets/images/logo.png" alt="logo"></a>
                </div><!-- / navbar-header -->
                <div class="secondary-nav">
                    <?php                    
                    if(isset($_SESSION['logged_in'])){
                        echo ' <a href="'.base_url('account/logout"').'" class="my-account space-right"><i class="fa fa-user"></i> Logout</a>';
                        if($_SESSION['check']== 99){
                             echo ' <a href="'.base_url('admin/dashboard"').'" class="btn btn-xs btn-primary"><i class="fa fa-user"></i> Dashboard</a>';
                         }                       

                    }else{                    
                        echo'  <a href="'.base_url('account').'" class="my-account space-right"><i class="fa fa-user"></i> Login </a>';
                    }

                    ?>
                </div>
                <div class="navbar-collapse collapse text-center">
                    <?php
                    $parntclass= array('nav','navbar-nav','main-menu');
                    $parntclass1= array('nav','navbar-nav','allcategorydrop');

                    $childclass= array('');
                    $parentli = array('dropdown');
                    $innerul = array('dropdown-menu','animated','zoomIn','fast');
                    // MenuHtml('main menu',$parntclass,$childclass,$parentli,$innerul);
                     get_categories($parntclass1,$childclass,$parentli,$innerul);
                    

                       ?>
                    
                </div>

                <!--/ nav-collapse -->
            </div><!-- / container -->
        </nav>
        <!-- / nav -->

        <!-- header-banner -->
        <div id="header-banner" class="demo-1">
            <div class="banner-content text-center">
                <div class="banner-border">
                    <div class="banner-info">
                        <h1>Minimal Shop</h1>
                        <p>Minimal Fashion Shop</p>
                    </div><!-- / banner-info -->
                </div><!-- / banner-border -->
            </div><!-- / banner-content -->
        </div>
        <!-- / header-banner -->
    </header>
    <!-- / header -->
