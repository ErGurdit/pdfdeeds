<!-- shop right-sidebar -->
<section id="shop" class="space-top-30">
    <div class="container">
        <div class="row">

            <div class="col-sm-8 col-md-9 content-area">
                <p class="shop-results space-left">Showing <strong class="showing"><?=$limit;?></strong> of <strong><?=$total;?></strong> items.                    
                </p>
                <div class="row shop list-unstyled" id="load-more">

                <?php 
                    if(!empty($books)){
                        $count = 0;
                         $lastid = end($books)['id'];
                        foreach ($books as $book) {
                      $cover_image = base_url('assets/images/m-product1.jpg');
                        if(!empty($book['cover_image'])){
                            $path = base_url('bookscover/'.$book['cover_image']);
                                $cover_image = $path;                             
                                            
                         }
                    ?>                              
                        <div class="col-xs-3 product m-product" data-groups='["bedroom"]'>
                            <div class="img-bg-color primary">
                                <a href="single-product2.html" class="product-link"></a>
                                <!-- / product-link -->
                                <img style="width:265px; height: 350px;" src="<?= $cover_image;?>" alt="">
                                <!-- / product-image -->

                                <!-- product-hover-tools -->
                                <div class="product-hover-tools">
                                    <a href="single-product2.html" class="view-btn" data-toggle="tooltip" title="View Product">
                                        <i class="lnr lnr-eye"></i>
                                    </a>
                                    <a target="_blank" href="<?= filedownloadpath($book['file']);?>" class="cart-btn" data-toggle="tooltip" title="Dwonload File">
                                        <i class="fa fa-file"></i>
                                    </a>
                                </div><!-- / product-hover-tools -->

                                <!-- product-details -->
                                <div class="product-details">
                                    <h5 class="product-title"><?= $book['name'];?></h5>
                                    <p class="product-category"><?= $book['category'];?></p>
                                </div><!-- / product-details -->
                            </div><!-- / img-bg-color -->
                        </div>
                                            <!-- / product -->               
                        <?php $count++; }

                        }else{
                            echo("<h1>No Book Fround For this category</h1>");
                            }?>                      

                </div> <!-- / products -->

                <div class="text-center more-button space-top-30">
                    <button onclick="loadmore()" class="btn btn-default-filled loadmorebtn">
                        <i class="fa fa-spinner fa-spin"></i><span>LOAD MORE</span></button>
                    <input type="hidden" name="urlid" id="urlid" value="<?=$urlid?>">
                    <input type="hidden" name="limit" id="limit" value="<?=$limit?>"/>
                <!--     <input type="hidden" name="lastid" id="lastid" value="<?=$lastid?>"/> -->
                    <input type="hidden" name="offset" id="offset" value="<?=$offset?>"/>
                </div>

            </div><!-- / content-area -->

            <div class="col-sm-4 col-md-3 sidebar-area">

                <!-- categries widget -->
                <div class="categories-sidebar-widget widget no-border">
                    <h5 class="widget-title">CATEGORIES</h5>

                    <?php 
                    $get_parent_class=array('category-ul');
                    $childclass=array('product-category');
                    $parentli=array('parentli');
                    $innerul=array('innerul');
                    get_categories($get_parent_class,$childclass,$parentli,$innerul); ?>

                </div>
                <!-- / categories-sidebar-widget -->
            </div><!-- / sidebar-area -->

        </div><!-- / row -->
    </div><!-- / container -->
</section>
<!-- / shop right sidebar -->
