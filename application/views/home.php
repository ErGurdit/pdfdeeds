<!-- features section 3col -->
<!-- shop section -->
<section id="shop">
    <div class="container">
        <div class="page-header no-margin-top text-center">
            <br>
            <br>
            <h3>LATEST Books</h3>
        </div>
        <!--/ page-header -->
        <ul class="row shop list-unstyled" id="grid">
            <!-- product -->

        <?php 
        $books = Books();
        if(!empty($books)){
            $count = 0;
            foreach ($books as $book) {
                if( $count == 4){
                  break;
                }
            $cover_image = base_url('assets/images/m-product1.jpg');
            if(!empty($book['cover_image'])){
                $path = base_url('bookscover/'.$book['cover_image']);
                    $cover_image = $path;
                                
             }
        ?>
            <li class="col-xs-6 col-md-3 product m-product" data-groups='["mens"]'>
                <div class="img-bg-color primary">
                    <a href="#" class="product-link"></a>
                    <!-- / product-link -->
                    <img style="width:265px; height: 350px;" src="<?= $cover_image;?>" alt="">
                    <!-- / product-image -->
                    <!-- product-hover-tools -->
                    <div class="product-hover-tools">
                        <a href="single-product.html" class="view-btn" data-toggle="tooltip" title="View Product">
                            <i class="lnr lnr-eye"></i>
                        </a>
                        <a  target="_blank" href="<?= filedownloadpath($book['file']);?>" class="cart-btn" data-toggle="tooltip" title="Dwonload File">
                            <i class="fa fa-file"></i>
                        </a>
                    </div><!-- / product-hover-tools -->

                    <!-- product-details -->
                    <div class="product-details">
                        <h5 class="product-title"><?=$book['name']?></h5>
                        <p class="product-category"><?=$book['category']?></p>
                    </div><!-- / product-details -->
                </div><!-- / img-bg-color -->
            </li>
            <!-- / product -->
        <?php  $count  ++;}
    }?>
            <!-- sizer -->
            <li class="col-xs-6 col-md-3 shuffle_sizer"></li>
            <!-- / sizer -->

        </ul> <!-- / products -->
    </div><!-- / container -->

</section>
<!-- / shop section -->

<!-- blog section 3col -->
<section id="blog-small">
    <div class="container">
        <div class="page-header no-margin text-center">
            <h3>LATEST POSTS</h3>
        </div>
        <p class="text-center space-top">Sed imperdiet vitae ipsum a tincidunt. Vivamus id ligula turpis. Duis viverra arcu quis.</p>
        <div class="row">

            <!-- post-block -->
            <div class="col-sm-4">
                <div class="post-block">
                    <a href="single-post.html"><img src="<?= base_url();?>assets/images/post-img1.jpg" alt=""></a>
                    <div class="small-post-text">
                        <h5><a href="single-post.html">SINGLE POST</a></h5>
                        <p class="small-post-meta">AUG 16, 2016</p>
                        <p class="space-bottom">Suspendisse in mattis neque, sed accumsan erat. Maecenas eget metus dui. Vestibulum accumsan massa quam...</p>
                        <p class="small-post-footer">
                            <a href="single-post.html">READ MORE</a>
                            <span class="post-icons pull-right">
                                <a href="#x"><i class="lnr lnr-thumbs-up"></i></a>
                                <a href="#x"><i class="lnr lnr-bubble"></i></a>
                            </span>
                        </p>
                    </div><!-- / small-post-text -->
                </div><!-- / post-block -->
            </div><!-- / col-md-4 -->
            <!-- / post-block -->

            <!-- post-block -->
            <div class="col-sm-4">
                <div class="post-block">
                    <a href="single-post-full.html"><img src="<?= base_url();?>assets/images/post-img2.jpg" alt=""></a>
                    <div class="small-post-text">
                        <h5><a href="single-post-full.html">FULLWIDTH POST</a></h5>
                        <p class="small-post-meta">AUG 16, 2016</p>
                        <p class="space-bottom">Suspendisse in mattis neque, sed accumsan erat. Maecenas eget metus dui. Vestibulum accumsan massa quam...</p>
                        <p class="small-post-footer">
                            <a href="single-post-full.html">READ MORE</a>
                            <span class="post-icons pull-right">
                                <a href="#x"><i class="lnr lnr-thumbs-up"></i></a>
                                <a href="#x"><i class="lnr lnr-bubble"></i></a>
                            </span>
                        </p>
                    </div><!-- / small-post-text -->
                </div><!-- / post-block -->
            </div><!-- / col-md-4 -->
            <!-- / post-block -->

            <!-- post-block -->
            <div class="col-sm-4">
                <div class="post-block">
                    <a href="single-post.html"><img src="<?= base_url();?>assets/images/post-img3.jpg" alt=""></a>
                    <div class="small-post-text">
                        <h5><a href="single-post.html">POST WITH SIDEBAR</a></h5>
                        <p class="small-post-meta">AUG 16, 2016</p>
                        <p class="space-bottom">Suspendisse in mattis neque, sed accumsan erat. Maecenas eget metus dui. Vestibulum accumsan massa quam...</p>
                        <p class="small-post-footer">
                            <a href="single-post.html">READ MORE</a>
                            <span class="post-icons pull-right">
                                <a href="#x"><i class="lnr lnr-thumbs-up"></i></a>
                                <a href="#x"><i class="lnr lnr-bubble"></i></a>
                            </span>
                        </p>
                    </div><!-- / small-post-text -->
                </div><!-- / post-block -->
            </div><!-- / col-md-4 -->
            <!-- / post-block -->

        </div><!-- / row -->
    </div><!-- / container -->
</section>
<!-- / blog section 3col -->

<!-- brands carousel -->
<section id="brands" class="dark primary-background">
    <h2 class="hidden">&nbsp;</h2>
    <div class="container">
        <div id="brands-carousel" class="owl-carousel">
            <div class="item"><img src="<?= base_url();?>assets/images/brand1.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand2.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand3.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand4.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand5.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand6.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand7.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand8.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand9.png" alt=""></div>
            <div class="item"><img src="<?= base_url();?>assets/images/brand10.png" alt=""></div>
        </div>
    </div><!-- / container -->
</section>
<!-- / brands carousel -->

<!-- / content -->
