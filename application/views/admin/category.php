<div class="row">
    <!-- main controls -->
    <div class="col-xs-12">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <form id="categoryform" role="form" action="<?= base_url('index.php/category/add')?>" method="post">
                    <div class="form-body ">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group ">
                                    <label for="cat_name">Name</label>
                                    <input name="name" type="text" id='cat_name' class="form-control" placeholder="Enter your name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group  has-info">
                                    <label for="">Choose Parent Category</label>
                                    <input type="text" id="categories" placeholder="Select">
                                    <input type="hidden" class="getids" name='parent'>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-actions noborder">
                                    <button type="submit" id="submitbtn" class="btn blue">Add</button>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- main controls -->

    <!-- all categories side -->
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bubble font-purple"></i>
                    <span class="caption-subject font-purple sbold uppercase">All Categories</span>
                </div>

            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="category_table">
                    <thead>
                        <tr>
                            <th> Sno </th>
                            <th> Category Name </th>
                            <th> Parent </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- All categories ends here -->
</div> <!-- row end here -->
<!-- the form to be viewed as dialog-->

<!-- Modal -->
<div id="catmodel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Category</h4>
            </div>
            <div class="modal-body">
                <!-- Modal content from /views/ajax/cat_model-->
            </div>
        </div>

    </div>
</div>
