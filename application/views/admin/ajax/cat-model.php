<form id="update_cat" role="form" action="" method="post">
    <div class="form-body ">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group ">
                    <label for="cat_name">Name</label>
                    <input name="name" type="text" value="<?php check_if_value($data['name']); ?>" id='e_cat_name' class="form-control" placeholder="Enter your name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group  has-info">
                    <label for="">Choose Parent Category</label>
                    <input type="text" id="categories2" placeholder="Select">
                    <input type="hidden" class="getids" name='parent' value='<?php  check_if_value($data['parent'] ); ?>'>
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                <div class="form-actions noborder text-center">
                    <button type="submit" id="e_submitbtn" class="btn blue">Update</button>
                    <input id="selectedcat" type="hidden" value='<?php  check_if_value($data['parent'] ); ?>'>
                    <input type="hidden" name="edit" name='edit' id="edit" value='<?php echo $data['id']?>'>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>
