<form id="menulinkUpdate" method="post">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Link Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Link Name</label>
                        <input type="text" class="form-control" value="<?= $link['name'] ?>" name="name" placeholder="Enter your name">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">url</label>
                        <input type="text" class="form-control"  value="<?= $link['slug'] ?>" name="slug" placeholder="Enter your name">
                    </div>
                </div>
            </div> <!-- rowends-->
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Update</button>
            <input type="hidden" name="edit"  value="<?= $link['id'] ?>">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</form>