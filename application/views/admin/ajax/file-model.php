
<?php
 $catids = (json_encode(explode(',',$data['category'])));
?>
<form method='post' id="update" action='<?= base_url('index.php/file/update')?>'
    enctype='multipart/form-data'>
    <div class="row">
        <div class="col-xs-12">
           
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Book Name </label>
                            <input type="text" class="form-control" name="name" value="<?php check_if_value($data['name']); ?>" placeholder="Book Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Book cover <a class="btn btn-xs btn-success" onclick="view_image('<?=base_url('bookscover/'.$cover_image)?>')">View image</a> </label> 
                            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#coverimg">Upload Image</button>                       
                            <input type="hidden" class="bookcover" name="cover_image" value="<?php check_if_value($cover_image); ?>">
                            <input type="hidden"  name="old_cover_image" value="<?php check_if_value($cover_image); ?>">
        
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Author Name </label>
                            <select class="form-control author"  name="author">
                            <?php
                            if(!empty($authors)){
                                foreach ($authors as $key => $value) {
                                 echo "<option value='".$value['id']."'>".$value['name']."</option>"; 
                                }                               
                            }
                            ?>
                            <option value="">Other</option>
                            </select>                            
                        </div>
                    </div>            
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Pusblisher name</label>
                            <select class="form-control publisher" name="publisher">
                            <?php
                            if(!empty($publishers)){
                                foreach ($publishers as $key => $value) {
                                 echo "<option value='".$value['id']."'>".$value['name']."</option>"; 
                                }                               
                            }
                            ?>
                            <option value="">Other</option>
                            </select>                            
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Language</label>
                            <select name='lang' class='form-control'>
                                <option <?php echo ($data['lang']=='punjabi')?'selected':''; ?> value='punjabi' >Punjabi</option>
                                <option <?php echo ($data['lang']=='hindi')?'selected':''; ?>  value='hindi'>Hindi</option>
                                <option <?php echo ($data['lang']=='english')?'selected':''; ?>  value='english'>English</option>                                
                            </select>
                         </div>
                    </div>
                </div>
        </div>
        <!-- left side -->
        <div class="col-md-12">
                <div class="form-group">
                    <label for="">Choose Categories</label>
                    <input type="text" id="categories2" placeholder="Select">
                </div>
                <div class="form-group">
                    <label for="">Book Discription </label>
                    <textarea class="form-control" resize="none" name="description" placeholder="Book Discription"><?php check_if_value($data['description']); ?></textarea>
                </div>
        </div>
        <!-- left side ends here -->
        <!-- right side -->
        <div class="col-xs-12 col-md-12">
            <div class="form-actions noborder text-center">
                <button type="submit" id="e_submitbtn" class="btn blue">Update</button>
                <input id="selectedcat" type="hidden" value='<?php  check_if_value($catids ); ?>'>
                <input type="hidden" class="getids" name='category' >
                <input type="hidden" name="edit" name='edit' id="edit" value='<?php echo $data['id']?>'>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- right side ends here -->
    </div> <!-- row end here -->
</form>
