       </div> <!-- page content wrapper -->
       </div> <!-- page conttent in heaser  -->
       </div><!-- end page content -->
       <!-- BEGIN FOOTER -->
       <div class="page-footer">
           <div class="page-footer-inner">Daljeet Singh
           </div>
           <div class="scroll-to-top">
               <i class="icon-arrow-up"></i>
           </div>
       </div>
       <!-- END FOOTER -->
       </div>

       <!-- BEGIN CORE PLUGINS -->
       <script>
           var base_url = "<?= base_url('/admin/')?>";

       </script>
       <!-- 
        <script src="<?= base_url()?>assets/admin_assets/global/plugins/jquery.min.js" type="text/javascript"></script>     -->
       <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
       <script src="<?= base_url()?>assets/admin_assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js" type="text/javascript"></script>
       <script src="<?= base_url()?>assets/admin_assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
       <script src="<?= base_url()?>assets/admin_assets/global/scripts/app.min.js" type="text/javascript"></script>
       <script src="<?= base_url()?>assets/admin_assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
       <script src="<?= base_url()?>assets/admin_assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
       <script src="<?= base_url()?>assets/admin_assets/datatable.js" type="text/javascript"></script>
       <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
       <script src="<?= base_url()?>assets/admin_assets/common-functions.js" type="text/javascript"></script>
       <!-- include js file acording to controller -->

       <?php
         $filepath = "application/views/admin/pages_js/".controller_name().'_js.php';
         if(file_exists($filepath)){
           include_once($filepath);
         }
        ?>
       <!-- include js file acording to controller ends here-->

       <script src="<?= base_url()?>assets/admin_assets/functions.js" type="text/javascript"></script>
       </body>
