  <!-- BEGIN SIDEBAR -->

  <div class="page-sidebar-wrapper">
      <div class="page-sidebar navbar-collapse collapse">
          <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
              <li class="nav-item start <?= (controller_name()=='dashboard'?'active':'');?> ">
                  <a href="<?= base_url('/admin/dashboard')?>" class="nav-link ">
                      <i class="icon-bar-chart"></i>
                      <span class="title">Dashboard</span>
                  </a>
              </li>
              <!--<li class="nav-item start <?= (controller_name()=='menu'?'active':'');?> ">
                  <a href="<?= base_url('/admin/menu')?>" class="nav-link ">
                      <i class="icon-bar-chart"></i>
                      <span class="title">Menu</span>
                  </a>
              </li>-->
              <li class="nav-item start <?= (controller_name()=='category'?'active':'');?>">
                  <a href="<?= base_url('/admin/category')?>" class="nav-link ">
                      <i class="icon-bar-chart"></i>
                      <span class="title">Categorires</span>
                  </a>
              </li>
              <li class="nav-item start <?= (controller_name()=='file'?'active':'');?> ">
                  <a href="<?= base_url('/admin/file')?>" class="nav-link ">
                      <i class="icon-bar-chart"></i>
                      <span class="title">File</span>
                  </a>
              </li>
              <li class="nav-item start <?= (controller_name()=='users'?'active':'');?> ">
                  <a href="<?= base_url('/admin/users')?>" class="nav-link ">
                      <i class="icon-bar-chart"></i>
                      <span class="title">All Users</span>
                  </a>
              </li>
               <li class="nav-item start <?= (controller_name()=='info'?'active':'');?> ">
                  <a href="<?= base_url('/admin/info')?>" class="nav-link ">
                      <i class="icon-bar-chart"></i>
                      <span class="title">File Info</span>
                  </a>
              </li>

          </ul>
          <!-- END SIDEBAR MENU -->
          <!-- END SIDEBAR MENU -->
      </div>
      <!-- END SIDEBAR -->
  </div>
  <!-- END SIDEBAR -->
