<script src="<?= base_url()?>assets/admin_assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<script src="<?= base_url()?>assets/admin_assets/combo-tree.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin_assets/croppe.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {

    /*alert_message function is in common-function.js*/
    
    demoUpload();
    manage_fileinfo();

/*datatable init */

 var table = 	 $('#files_table').DataTable( {
        "ajax": base_url+'datatable/files'
    } );

/*end with datatable init */

$('#fileUploadForm').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);
    $.ajax({
            url:  base_url+"file/filedata",
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response){
              var obj = jQuery.parseJSON(response);
              console.log(obj);
              alert_message(obj,table,'#fileUploadForm');
            },
        });
  })

/*stop preventing from submition */
$('#categoryform').submit(function(e){
	e.preventDefault();	
});
/*end with form submittion part*/

/*validation */
var validator =    $('#categoryform').validate({ // initialize the plugin
    rules: {
        name: {
            required: true
        }
    },
     submitHandler: function(form) {  
    	var formData =  $('#categoryform').serialize();
		$.post( base_url+"file/add",formData,function(response) {
			var obj = jQuery.parseJSON(response);
			console.log(obj.inputs);
		   alert_message(obj,table,'#categoryform');
		});
    }
       
});
/*end with validation*/

/*delete tabel row*/
$('#files_table').on('click','.delete' ,function(){
	 var id = $(this).attr('data-id');
	 alertify.confirm('Delete Book', 'Do You want to delete this Book', 
		function(){
			$.post( base_url+"file/delete",{'id':id},function(response) {
				var obj = jQuery.parseJSON(response);
				alert_message(obj,table);
			});
		},
		function(){
			alertify.error('Cancel');
		});
	
});
/*delete tabel row*/

/*delete tabel row*/
$('#files_table').on('click','.edit' ,function(){
	 var id = $(this).attr('data-id');
	 $('#catmodel').modal("show");
	 $.post( base_url+"file/edit",{'id':id},function(response) {
		$('#catmodel .modal-body').html(response);
        update_cat();
        manage_fileinfo();
        all_categories_tree('#categories2');
        
	});
});
/*delete tabel row*/
function update_cat(){
	
$('#update').on('submit',function(e){
	e.preventDefault();	
	var formData =  $('#update').serialize();	
		$.post( base_url+"file/update",formData,function(response) {
			var obj = jQuery.parseJSON(response);
			// var errorInp = obj.inputs;
			// $.each(errorInp,function(i, value){
			//  $("input[name='"+i+"']").addClass('input_error');
            // });
			$('#catmodel').modal("hide");
		   alert_message(obj,table,'#update');
		});
})
}




   
	
}); /*document ready colse */
function all_categories_tree(id){
   
    $.getJSON( base_url+"category/category_tree_options",function(response) { 
               
    var comboTree1  = $(id).comboTree({
            source : response,
            isMultiple: true,
            //selected: selectedIds,
            hiddeninput:'.getids'
        });
        if($('#selectedcat').val()){
            var selectedIds = JSON.parse($('#selectedcat').val());  
            comboTree1.setSelection(selectedIds);
        }
       
    });
    
   
}
all_categories_tree('#categories');

function demoUpload() {
		var $uploadCrop;

		function readFile(input) {
 			if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	            	$uploadCrop.croppie('bind', {
	            		url: e.target.result
	            	}).then(function(){
	            		console.log('jQuery bind complete');
	            	});	            	
	            }
	            
	            reader.readAsDataURL(input.files[0]);
	        }
	        else {
		        swal("Sorry - you're browser doesn't support the FileReader API");
            }

            $uploadCrop = $('#upload-demo').croppie({
                viewport: {
                    width: 160,
                    height: 250
                },
                enableExif: true
            });
            $('.upload-msg').hide();
            $('.upload-demo-wrap').show();
		}

		

		$('#upload').on('change', function () { readFile(this); });
		$('.upload-result').on('click', function (ev) {
			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp) {
                $('.bookcover').val(resp);
                alertify.alert(
                    "<b>Your image</b>",
                    "<img style='width:160px;height:250px;margin: auto;display: block;border: 1px solid #eaeaea;' src='"+resp+"'/>"
                );
			});
		});
	}
 
 function view_image(src){
	alertify.alert(
		"<b>Your image</b>",
		"<img title='No Image Found' style='width:160px;height:250px;margin: auto;display: block;border: 1px solid #eaeaea;' src='"+src+"'/>"
	);
 }


function manage_fileinfo(){
	$('.author,.publisher').on('change', function () {
		var inpvalue = $(this).val();
		var promptname =  $(this).attr('name');
		if(inpvalue==''){
			alertify.prompt( promptname+'Name ', 'Please enter '+promptname+' name', ''
	           ,function(evt, value) { 
		           	if(value){
		           		$.post( base_url+"info/add",{'name':value,'type':promptname},function(response) {
							var obj = jQuery.parseJSON(response);
							alert_message(obj);
			           	});
			           	$.post(base_url+"info/options",{'type':promptname},function(html){
			           		$('.'+promptname).html(html);
			           	});
		           	}else{
		           		alertify.error('feild Can not be empty');
		           	}
	           	 
	           }, function() {
	            
	           });
		}
	});
}




</script>