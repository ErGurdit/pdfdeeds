 <script src="<?= base_url()?>assets/admin_assets/global/scripts/datatable.js" type="text/javascript"></script>
 <script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
 <script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

 <script type="text/javascript">
     $(document).ready(function() {

         /*alert_message function is in common-function.js*/

         /*datatable init */

         var table = $('#author_datatable').DataTable({
             "ajax": base_url + 'datatable/info?type=author'
         });

         var table1 = $('#publisher_datatable').DataTable({
             "ajax": base_url + 'datatable/info?type=publisher'
         });

         /*end with datatable init });
*/

         /*stop preventing from submition */
         $('#categoryform').submit(function(e) {
             e.preventDefault();
             var formData = $(this).serialize();
             $.post(base_url + "info/add", formData, function(response) {
                 var obj = jQuery.parseJSON(response);
                 table.ajax.reload();
                 table1.ajax.reload();
                 alert_message(obj,'', '#categoryform');
             });
         });
         /*end with form submittion part*/
         

         /*delete tabel row*/
         $('#author_datatable ,#publisher_datatable').on('click', '.delete', function() {
             var id = $(this).attr('data-id');
              alertify.confirm('Delete Menu', 'Do You want to delete this menu',
             function() {
                 $.post(base_url + "info/delete", {
                     'id': id
                 }, function(response) {
                     var obj = jQuery.parseJSON(response);
                     table.ajax.reload();
                     table1.ajax.reload();
                     alert_message(obj);
                 });
            },
            function() {
            });

             
         });
         /*delete tabel row*/

         /*update tabel row*/
         $('#author_datatable ,#publisher_datatable').on('click', '.edit', function() {            
             var id = $(this).attr('data-id');
            var name = $(this).attr('feildsname');
            alertify.prompt('Change Name', 'Enter  Name', name, function(evt, value) {
                if(value){
                    $.post(base_url + "info/update",{'id':id,'name':value}, function(response) {
                    var obj = jQuery.parseJSON(response);
                     table.ajax.reload();
                     table1.ajax.reload();
                    alert_message(obj);
                 });
                }else{
                    alertify.error('Author name can not be empty');
                }
                
            }, function() {
               
            });
         });
         /*update tabel row*/
      
     }); /*document ready colse */

    

 </script>
