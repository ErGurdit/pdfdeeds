<script src="<?= base_url()?>assets/admin_assets/global/scripts/datatable.js" type="text/javascript"></script>
 <script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
 <script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <script type="text/javascript">
     $(document).ready(function() {

         /*datatable init */

         var table = $('#category_table').DataTable({
             "ajax": base_url + 'datatable/users'
         });

         /*end with datatable init */



         /*delete tabel row*/
         $('#category_table').on('click', '.delete', function() {
            var id = $(this).attr('data-id');
            alert(id);
            alertify.confirm('Delete User', 'Do You want to delete this User',
            function() {
                $.post(base_url + "users/delete", {
                    'id': id
                }, function(response) {
                    var obj = jQuery.parseJSON(response);
                    alert_message(obj, table);
                });
            },
            function() {
                
            });

            
         });
         /*delete tabel row*/

         $(document).on('click', '.link_visible', function() {
            var id = $(this).attr('data-id');
            var is_visible = $(this).attr('status');
            $.post(base_url + "users/update", {
                'id': id,
                'is_active': is_visible,
                'visible_status': 1
            }, function(response) {
                var obj = jQuery.parseJSON(response);
                alert_message(obj,table);
                load_menu_list($('#selected_menu').val())
            });
        });

     }); /*document ready colse */

   
 </script>
