<script src="<?= base_url()?>assets/admin_assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
    type="text/javascript"></script>
<script>
$(document).ready(function() {


    /*datatable init */

    var table = $('#datatable').DataTable({
        "ajax": base_url + 'datatable/menu'
    });


    /*delete tabel row*/
    $('#datatable').on('click', '.delete', function() {
        var id = $(this).attr('data-id');
        alertify.confirm('Delete Menu', 'Do You want to delete this menu',
            function() {
                $.post(base_url + "menu/delete", {
                    'id': id,
                    'delete_menu': 1
                }, function(response) {
                    var obj = jQuery.parseJSON(response);
                    alert_message(obj, table);
                });
            },
            function() {
                alertify.error('Cancel');
            });

    });
    /*delete tabel row*/

    /*delete tabel row*/
    $('#datatable').on('click', '.edit', function() {
        var id = $(this).attr('data-id');
        var name = $(this).attr('menuname');
        alertify.prompt('Change Name', 'Enter Menu Name', name, function(evt, value) {
            if(value){
                $.post(base_url + "menu/update",{'id':id,'change_name':value}, function(response) {
                var obj = jQuery.parseJSON(response);
                alert_message(obj,table);
             });
            }else{
                alertify.error('Menu name can not be empty');
            }
            
        }, function() {
           
        });
    });
    /*delete tabel row*/
    /*end with datatable init */
    load_menu_list($('#selected_menu').val()) //load menu on page load
    $('#menu_list').nestable();
    SaveMenuOrderOnChange();
    DeleteMenuLinkFromTree();
    VisibleMenuLinkFromTree();
    EditMenuLinkFromTree();
    UpdateMenuLink();
    //   init menu list
    $('#add_link').on('click', function() {
        // ++++++ open model box for menu item form  ++++++ //
        $('#myModal').modal();
        $('#get_menu').val($('#selected_menu').val()); // to save value in current selected menu
    });

    $('#selected_menu').on('change', function() {
        // ++++++ on Chnage Menu Load Menu Tree  ++++++ //
        load_menu_list($(this).val());
    })

    $('#menuform').submit(function(e) {
        // ++++++ add new menu ++++++  //
        e.preventDefault();
        var formData = $(this).serialize();
        $.post(base_url + "menu/add", formData, function(response) {
            var obj = jQuery.parseJSON(response);
            alert_message(obj,table, '#menuform');
            load_menu_options();
        });
    });


    $('#menuitem').submit(function(e) {
        // ++++++  add custom link for current menu ++++++ //
        e.preventDefault();
        var formData = $(this).serialize();
        $.post(base_url + "menu/AddMenuItem", formData, function(response) {
            var obj = jQuery.parseJSON(response);
            console.log(obj.inputs);
            load_menu_list($('#selected_menu').val()) //load menu on page load
            alert_message(obj, '', '#menuitem');
        });
    });
}); // end of document ready function

function load_menu_list(menuid) {
    // function to load menu tree on page
    $('#menu_list').load(base_url + "menu/menuitems", {
        'id': menuid
    });
}

function SaveMenuOrderOnChange() {
    // ++++++ Save Menu Tree Order  ++++++ //
    $('#menu_list').on('change', function() {
        var selected_menu = $('#selected_menu').val();
        var menudata = JSON.stringify($('#menu_list').nestable('serialize'));
        // console.log(menudata)
        $.post(base_url + "menu/savemenuorder", {
            'id': selected_menu,
            'menu_order': menudata
        }, function(response) {
            var obj = jQuery.parseJSON(response);
            alert_message(obj, '', '');
        });
    });
}

function DeleteMenuLinkFromTree() {
    $(document).on('click', '.link_delete', function() {
        var id = $(this).attr('id');
        alertify.confirm('Delete Link', 'Do You want to delete this Link',
            function() {
                $.post(base_url + "menu/delete", {
                    'id': id
                }, function(response) {
                    var obj = jQuery.parseJSON(response);
                    alert_message(obj);
                    if (obj.status == 'Success') {
                        $(this).closest('li').fadeOut('slow').remove();
                        load_menu_list($('#selected_menu').val());
                    }
                });
            },
            function() {

            });
    });
}

function VisibleMenuLinkFromTree() {
    $(document).on('click', '.link_visible', function() {
        var id = $(this).attr('id');
        var is_visible = $(this).attr('status');
        $.post(base_url + "menu/update", {
            'id': id,
            'is_visible': is_visible,
            'visible_status': 1
        }, function(response) {
            var obj = jQuery.parseJSON(response);
            alert_message(obj);
            load_menu_list($('#selected_menu').val())
        });
    });
}

function EditMenuLinkFromTree() {
    $(document).on('click', '.link_edit', function() {
        $('#myModal').modal();
        var id = $(this).attr('id');
        $('#myModal .modal-dialog').load(base_url + "menu/LoadUpdatemodel", {
            'id': id
        });
    });
}

function UpdateMenuLink() {
    $(document).on('submit', '#menulinkUpdate', function(e) {
        // ++++++  Update custom link for current menu ++++++ //
        e.preventDefault();
        var formData = $(this).serialize();
        $.post(base_url + "menu/update", formData, function(response) {
            var obj = jQuery.parseJSON(response);
            load_menu_list($('#selected_menu').val()) //load menu on page load
            alert_message(obj);
            $('#myModal').modal('hide');
        });
    });
}

function load_menu_options(){
    $('#selected_menu').load(base_url + "menu/LoadMenuOptions");
}
</script>