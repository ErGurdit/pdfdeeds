 <script src="<?= base_url()?>assets/admin_assets/global/scripts/datatable.js" type="text/javascript"></script>
 <script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
 <script src="<?= base_url()?>assets/admin_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" type="text/javascript"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
 <script src="<?= base_url()?>assets/admin_assets/combo-tree.js" type="text/javascript"></script>

 <script type="text/javascript">
     $(document).ready(function() {

         /*alert_message function is in common-function.js*/

         /*datatable init */

         var table = $('#category_table').DataTable({
             "ajax": base_url + 'datatable/categories'
         });

         /*end with datatable init */

         /*stop preventing from submition */
         $('#categoryform').submit(function(e) {
             e.preventDefault();
         });
         /*end with form submittion part*/

         /*validation */
         var validator = $('#categoryform').validate({ // initialize the plugin
             rules: {
                 name: {
                     required: true
                 }
             },
             submitHandler: function(form) {
                 var formData = $('#categoryform').serialize();
                 $.post(base_url + "category/add", formData, function(response) {
                     var obj = jQuery.parseJSON(response);
                     console.log(obj.inputs);
                     alert_message(obj, table, '#categoryform');
                 });
             }

         });
         /*end with validation*/

         /*delete tabel row*/
         $('#category_table').on('click', '.delete', function() {
             var id = $(this).attr('data-id');
             $.post(base_url + "category/delete", {
                 'id': id
             }, function(response) {
                 var obj = jQuery.parseJSON(response);
                 alert_message(obj, table);
             });
         });
         /*delete tabel row*/

         /*delete tabel row*/
         $('#category_table').on('click', '.edit', function() {
             var id = $(this).attr('data-id');
             $('#catmodel').modal("show")
             $.post(base_url + "category/edit", {
                 'id': id
             }, function(response) {
                 $('#catmodel .modal-body').html(response);
                 update_cat();
                 all_categories_tree('#categories2');
             });
         });
         /*delete tabel row*/
         function update_cat() {

             $('#update_cat').on('submit', function(e) {
                 e.preventDefault();
                 var formData = $('#update_cat').serialize();
                 $.post(base_url + "category/update", formData, function(response) {
                     var obj = jQuery.parseJSON(response);
                     console.log(obj.inputs);
                     alert_message(obj, table);
                 });
             })
         }


     }); /*document ready colse */

     function all_categories_tree(id) {

         $.getJSON(base_url + "category/category_tree_options", function(response) {
             var select = '';
             if ($('#selectedcat').val()) {
                 select = $('#selectedcat').val();
             }
             var comboTree1 = $(id).comboTree({
                 source: response,
                 selected: [select],
                 hiddeninput: 'getids' //given di in combo-tree.js file line: 356
             });

         });


     }
     all_categories_tree('#categories');


     //datatable('#category_table',base_url+'datatable/categories','gs_categories','');

 </script>
