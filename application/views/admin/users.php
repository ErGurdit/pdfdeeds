<div class="row">
   <!-- all categories side -->
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bubble font-purple"></i>
                    <span class="caption-subject font-purple sbold uppercase">All Users</span>
                </div>

            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="category_table">
                    <thead>
                        <tr>
                            <th> Sno </th>
                            <th>  Name </th>
                            <th> email </th>
                            <th>Phone</th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- All categories ends here -->
</div> <!-- row end here -->
<!-- the form to be viewed as dialog-->
