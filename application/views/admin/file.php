<!-- <?php 
pr($author);
?> -->
 <div class="portlet light bordered">
<form method='post' id="fileUploadForm" action='<?= base_url('index.php/file/filedata')?>'
    enctype='multipart/form-data'>
    <div class="row">
        <div class="col-xs-12">
                <div class="row row-flex ">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Book Name </label>
                            <input type="text" class="form-control" name="name" placeholder="Book Name">
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Author Name </label>
                            <select class="form-control author"  name="author">
                            <?php
                            if(!empty($authors)){
                                foreach ($authors as $key => $value) {
                                 echo "<option value='".$value['id']."'>".$value['name']."</option>"; 
                                }                               
                            }
                            ?>
                            <option value="">Other</option>
                            </select>                            
                        </div>
                    </div>            
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Pusblisher name</label>
                            <select class="form-control publisher" name="publisher">
                            <?php
                            if(!empty($publishers)){
                                foreach ($publishers as $key => $value) {
                                 echo "<option value='".$value['id']."'>".$value['name']."</option>"; 
                                }                               
                            }
                            ?>
                            <option value="">Other</option>
                            </select>                            
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Language</label>
                            <select name='lang' class='form-control'>
                                <option value='punjabi'>Punjabi</option>
                                <option value='hindi'>Hindi</option>
                                <option value='english'>English</option>                                
                            </select>
                         </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Choose Categories</label>               
                            <input type="text"  id="categories" placeholder="Select">
                            <input type="hidden" class="getids" name='category' >
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Cover image</label>
                            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#coverimg">Upload Image</button>
                         </div>
                    </div>
                </div>
               
        </div>
     
        <!-- left side -->
        <div class="col-md-4">
                <div class="form-group">
                    <label for="">Book Discription </label>
                    <textarea class="form-control" resize="none" name="description" placeholder="Book Discription"></textarea>
                </div>
        </div>
        <!-- left side ends here -->
        <!-- right side -->
        <div class="col-md-8">
            <div id="progress">
                <div class="bar" style="width: 0%;"></div>
            </div>
            <div class="form-group">
                <label for="">Choose File</label>
                <input  name="files[]"  class="form-control" id='filedata' type="file" multiple>
            </div>
            <br>
            <input type="hidden" name="bookcover" class='bookcover'>
            <button  class="btn btn-success center-block" type="submit">Submit</button>
        </div>
        <!-- right side ends here -->
    </div> <!-- row end here -->
</form>
</div>
 <div class="row">
    <!-- All Books side -->
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bubble font-purple"></i>
                    <span class="caption-subject font-purple sbold uppercase">All Books</span>
                </div>
                
            </div>
            <div class="portlet-body">
                 <table class="table table-striped table-bordered table-hover table-checkable order-column" id="files_table">
                    <thead>
                        <tr>
                            <th> Sno </th>
                            <th> Book Name </th>
                            <th> Author </th>
                            <th> Publisher</th>
                            <th> Language</th>
                            <th> Category</th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- All Books ends here -->
    </div> <!-- row end here -->
    <!-- the form to be viewed as dialog-->

<!-- Modal -->
<div id="catmodel" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title text-center">Update Info</h4>
    </div>
    <div class="modal-body">
        <!-- Modal content from /views/ajax/cat_model-->        

    </div>
</div>

</div>
</div>


<!-- Modal -->
<div id="coverimg" class="modal fade" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title text-center">Update Info</h4>
    </div>
    <div class="modal-body">
       <div class="row">
        <div class="col-sm-12">
            <div class="col-1-2">
                <div class="upload-msg">
                    Upload a file to start cropping
                </div>
                <div class="upload-demo-wrap">
                    <div id="upload-demo"></div>
                </div>
            </div>   
            <div class="actions">
                <a class="btn file-btn">
                    <span>Upload</span>
                    <input type="file" class='form-control' id="upload" value="Choose a file" accept="image/*" />
                </a>
                <button class="upload-result btn">Crop</button>
            </div>
        
        </div>  
        </div>
    </div>
</div>

</div>
</div>
