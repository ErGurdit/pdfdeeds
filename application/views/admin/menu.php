<div class="row">
    <!-- main controls -->
    <div class="col-xs-12">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <form role="form" id='menuform'>
                    <div class="form-body ">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Menu Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter your name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Choose Menu</label>
                                    <select name='selected_menu' class="form-control" id='selected_menu'>
                                        <?php All_menus(); //call helper function ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-actions noborder">
                                    <input type="hidden" name="menuorder">
                                    <button type="submit" class="btn blue">Submit</button>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- main controls -->
    <!-- left side -->
    <div class="col-md-4">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input has-info">
                                    <select class="form-control" name="category">
                                        <?php cat_options(); //helper function?>
                                    </select>
                                    <label for="">Choose Categories</label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-12">
                                <div class="form-actions noborder">
                                    <button type="button" id="add_link" class="btn blue btn-block">Add New Link</button>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- left side ends here -->
    <!-- right side -->
    <div class="col-md-8">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bubble font-purple"></i>
                    <span class="caption-subject font-purple sbold uppercase">Menu Order</span>

                </div>
                <form id="menuorderform">
                    <input type="hidden" name="menuorder" id="menuorder">
                    <input type="hidden" name="menuid">
                    <button class="btn btn-sm btn-success pull-right"><i class="fa fa-save"></i> Save Menu</button>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="portlet-body">
                <div class="dd" id="menu_list">

                </div>
            </div>
        </div>
    </div>
    <!-- right side ends here -->

    <!-- all Menu side -->
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bubble font-purple"></i>
                    <span class="caption-subject font-purple sbold uppercase">All Menus</span>
                </div>

            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column"
                    id="datatable">
                    <thead>
                        <tr>
                            <th> Sno </th>
                            <th> Menu Name </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- All menu ends here -->
</div> <!-- row end here -->




<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form id="menuitem" method="post">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Link Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Link Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Enter your name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">url</label>
                                <input type="text" class="form-control" name="slug" placeholder="Enter your name">
                            </div>
                        </div>
                    </div> <!-- row ends-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <input type="hidden" name="get_menu" id="get_menu">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>