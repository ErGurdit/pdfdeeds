<link rel="stylesheet" href="<?= base_url()?>assets/admin_assets/combo-tree.css">
<link rel="stylesheet" href="<?= base_url()?>assets/admin_assets/croppe.css">
<style>

.upload-demo .upload-demo-wrap,
.upload-demo .upload-result,
.upload-demo.ready .upload-msg {
    display: none;
}
.upload-demo.ready .upload-demo-wrap {
    display: block;
}
.upload-demo.ready .upload-result {
    display: inline-block;    
}
.upload-demo-wrap {
    width: 300px;
    height: 300px;
    margin: 0 auto;
    display:none;
}

.upload-msg {
    text-align: center;
    padding: 50px;
    font-size: 22px;
    color: #aaa;
    width: 260px;
    margin: 50px auto;
    border: 1px solid #aaa;
}
#coverimg .actions {
    margin-top: 35px;
}
a.btn.file-btn span {
    display: none;
}
.alertify .ajs-modal{
    z-index:9999999999999;
}
</style>