<div class="container">
    <div class="row">
        <?php if(empty($msg)){ ?>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    Welcome
                </div>
                <div class="card-body">
                    <h4 class="card-title"> <?= $_SESSION['name']; ?></h4>
                </div>
                <div class="card-footer text-muted">
                    <a name="" id="" class="btn btn-primary" href="<?=base_url('account/logout')?>" role="button">Logout</a>
                </div>
             </div>
        </div>
    <?php }else{ ?>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    Welcome
                </div>
                <div class="card-body">
                    <h4 class="card-title"> <?= $msg; ?></h4>
                </div>
             </div>
        </div>

    <?php } ?>
    </div>
</div>