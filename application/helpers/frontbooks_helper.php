<?php 
 function Books($where='',$find_in_set='',$limit='',$start='',$lastid=''){
	$CI = get_instance();
    $CI->load->model('Read_Model');
    if(!empty($where)){
    	$where = $where;
    }else{
    	$where = array('post_type'=>'books');
    }

    if(!empty($lastid)){
    	$where = array('post_type'=>'books','id <'=>$lastid);
    }

	$feilds = array( 'id','name', 'category','lang');
	$books= $CI->Read_Model->datareadIN('gs_posts',$feilds,$where,$find_in_set,'id','DESC',$limit,$start);

	$finaldata = array();
	if(!empty($books)){		
		
		foreach ($books as $row) {
			$catenames = '';
		$cats = explode(',', $row['category']);		
			foreach ($cats as  $value) {
				$where = array('id'=>$value);
				$catenames .= $CI->Read_Model->dataread('gs_categories','name',$where)[0]['name'].' ,';
			}		
		    $data['id']	= $row['id'];		
			$data['name']= ucfirst($row['name']);
			$data['author']=ucfirst($CI->Read_Model->postMetaValue($row['id'],'author'));
			$data['publisher']=ucfirst($CI->Read_Model->postMetaValue($row['id'],'publisher'));
			$data['lang']=ucfirst($row['lang']);
			$data['category']=rtrim($catenames, ",");
			$data['cover_image']=ucfirst($CI->Read_Model->postMetaValue($row['id'],'cover_image'));
			$data['file'] = $CI->Read_Model->postMetaValue($row['id'],'file');
	    	array_push($finaldata, $data);
		}
		return $finaldata;
	
	}else{
		return $finaldata;
	}
}


/*for caegory only*/

function get_categories($get_parent_class=array(),$childclass=array(),$parentli=array(),$innerul=array()){	
	$CI = get_instance();
    $CI->load->model('Read_Model');
    $get_feilds = array('id','name','parent');
    $data = $CI->Read_Model->dataread('gs_categories',$get_feilds,'parent=0');
   if($data){
   		get_category_html($data,$get_parent_class,$childclass,$parentli,$innerul);
   }
}

function get_category_html($data,$get_parent_class,$childclass,$parentli=array(),$innerul=array()){
	
    $CI = get_instance();
    $CI->load->model('Read_Model');
    
    $pclass = '';  // for main ul class
    $cClass = '';  // for li calss
    $child_class = ''; // for that li which contain dropdown ul class
    $get_parent_class1 = ''; // li parent ul class. classes for innerulonly
    $childclass1 = '';
    if(! empty($get_parent_class)){
    	$pclass =  implode(' ', $get_parent_class);
    } 
    if(!empty($childclass)){
    	 $cClass =  implode(' ', $childclass);
    }
    $count=1;
    $get_feilds = array('id','name','parent');
    if(!empty($data)){
		    echo  "<ul class='".$pclass."'>"; 		   
		   foreach($data  as  $value){    
		     	$childs =  $CI->Read_Model->dataread('gs_categories',$get_feilds,'parent='.$value['id']);
		   	  if(!empty($childs)){
			 			$child_class =  implode(' ', $parentli);
			 	}	
		   		echo "<li class='".$child_class .' '.$cClass. "' data-id='".$value['id']."'>
		        	<a href=".base_url('books/'.$value['id'])." > <span>	".$value['name']." </span></a>";        	
		      $child_class = '';
		      if(!empty($childs)){ 
		      	$get_parent_class = $innerul; //change the parent of ul for inner ul
		              get_category_html($childs,$get_parent_class,$childclass,$parentli,$innerul);              
		      }
		       
		   }
		  echo "  </ul>";
	}

}

function filedownloadpath($file){

	$path = base_url('/account');
	if(isset($_SESSION['logged_in'])){
		$path =  base_url('gs_uploads/'.$file);
	}
	return $path ;
}
/*for caegory only*/