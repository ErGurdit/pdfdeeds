<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 $order=1;

if ( ! function_exists('All_menus')){
    function All_menus($select = ''){
        // getting all avilable menus
        $CI = get_instance();
       $CI->load->model('Read_Model');
       $get_feilds = array('id','name');
       $menus=  $CI->Read_Model->dataread('gs_posts',$get_feilds,'post_type="menu"');  
        foreach ($menus as $key => $value) {
           $checked ='';
           if($select == $value['id']){
               $checked= "selected";
           }
           echo "<option value='".$value['id']."' $checked>".$value['name']."</option>";
       }
    }
 }
 function save_menu_order($menuorder){
    global $order;
    $CI = get_instance();
    $CI->load->model('Update_Model');
    foreach($menuorder as $menuitems){
        if($menuitems['id']){
            // section for setting up menu order
            $order ++; 
            $data_post = array('menu_order'=>$order);
            $CI->Update_Model->UPDATE($menuitems['id'],'gs_menu',$data_post);
        }
        if(isset($menuitems['children'])){
            // section for setup parent id in current child
            $childs = array();
            save_menu_order($menuitems['children']);
            foreach($menuitems['children'] as $childIs){
                $parents = array('parent'=> $menuitems['id']);
                $CI->Update_Model->UPDATE($childIs['id'],'gs_menu',$parents);
            }
           
        }else{
            // section for making parent id 0 if order is change of current id
            $parents = array('parent'=> 0);
            $CI->Update_Model->UPDATE($menuitems['id'],'gs_menu',$parents); 
        }
        
    }
 }

 function ChildLinks($data){
    //   Get all The Child Links from Menu Tree 
    $CI = get_instance();
    $CI->load->model('Read_Model');
    $html = "";
    $html .= "  <ol class='dd-list'>";
    foreach($data  as  $value){
        $class='';
        if($value['is_visible']==0){
            $class = 'disable';
        }
        $html .="<li class='dd-item' data-id='".$value['id']."'>
               
                 <div class='menu-list-block'>
                 <span class='dd-handle'> <i class='fa fa-arrows-alt'></i></span>
                 ".$value['name'] ."
                    <div class='action-control pull-right'>
                        <button type='button' class='btn btn-xs btn-default link_visible ".$class."' status='".$value['is_visible']."' id='".$value['id']."'><i class='fa fa-eye'></i></button>
                        <button type='button' class='btn btn-xs btn-success link_edit' id='".$value['id']."'><i class='fa fa-edit'></i></button>
                        <button type='button' class='btn btn-xs btn-danger link_delete' id='".$value['id']."' ><i class='fa fa-trash'></i></button>
                   </div>
                   <div class='clearfix'></div>
                   </div>  
               ";
           $childs =  $CI->Read_Model->dataread('gs_menu','','post_id='.$value['post_id']. ' and parent= '.$value['id'] ,'menu_order','asc');
           if($childs){
                $html .= ChildLinks($childs); // recall function if find child of current menu id $Value["id"]
            }
            $html .="</li>";
    }
    $html .= "</ol>";   
    return $html;
       
}
