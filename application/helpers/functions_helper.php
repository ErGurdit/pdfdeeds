<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( ! function_exists('random')){
   function random(){
         $number = rand(1111,9999).time();
         return $number;
       }
   }
 
if ( ! function_exists('current_date_time')){
   function current_date_time(){
         $dateTime = date('Y-m-d h:m:s ');
         return $dateTime;
       }
   }  



if ( ! function_exists('controller_name')){
   function controller_name(){
      $ci =& get_instance();
     return $ci->router->fetch_class();
    }
}   

if ( ! function_exists('pr')){
   function pr($data){
    echo "<pre>";
    print_r($data);
    echo "</pre>";

    }
}   

if ( ! function_exists('check_if_value')){
   function check_if_value($data){
      if($data){
         echo $data;
      }else{
         echo '';
      }

    }
}   

if ( ! function_exists('cat_options')){
   function cat_options($select = '')
   {      $CI = get_instance();
      $CI->load->model('Read_Model');
      $get_feilds = array('id','name','parent');
      $allcat=  $CI->Read_Model->dataread('gs_categories',$get_feilds);  
      echo '<option value="0">Choose Menu</option>';
       foreach ($allcat as $key => $value) {
          $checked ='';
          if($select == $value['id']){
              $checked= "selected";
          }
          echo "<option value='".$value['id']."' $checked>".$value['name']."</option>";
      }
   }
}

if ( ! function_exists('check_file')){
   function check_file($files,$allowed_files){
      $data  = true;
      if(!empty($files)){
         $files = (array_shift($files));
         foreach($files['name'] as $key => $file){
          $extention =  pathinfo( $file, PATHINFO_EXTENSION);
          if(!in_array($extention,$allowed_files)){
             $data = false;         
          }
        }
      }
     return $data;

   }
}

if ( ! function_exists('response_message')){   
   function response_message($data,$opration='updated'){
      if($data){
         $response =[
            'status'=>"Success",
            'msg'  => 'Data successfully '.$opration ,
            'inputs'=> ''
         ];
      }else{
         $response =[
            'status'=>"Error",
            'msg'  => 'query error',
            'inputs'=> ''
         ];
      }

      return $response;
   }
}


 function category_tree_json(&$data=array()){   
   $CI = get_instance();
   $CI->load->model('Read_Model');
   $mainarr = array();
   $get_feilds = array('id','name','parent');
   foreach($data  as $key => $value){      

      $mainarr[$key] =array('id'=>$value['id'],'title'=>$value['name']); 
      $childs =  $CI->Read_Model->dataread('gs_categories',$get_feilds,'parent='.$value['id']);
      if($childs){ 
         foreach($childs  as $k => $j){
            $mainarr[$key]['subs'][$k] =array('id'=>$j['id'],'title'=>$j['name']); 
            $ch =  $CI->Read_Model->dataread('gs_categories',$get_feilds,'parent='.$j['id']);
            if($ch){
               $mainarr[$key]['subs'][$k]['subs']= category_tree_json($ch);
            }          
         }
      
      }
         
   }
   return $mainarr;
}


function checkauth(){
  if(!isset($_SESSION['check']) || $_SESSION['check']!= 99){
        redirect(base_url());
  }
}