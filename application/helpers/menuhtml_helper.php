<?php

function MenuHtml($menuname='',$get_parent_class=array(),$childclass=array(),$parentli=array(),$innerul=array())
{   $CI = get_instance();
    $CI->load->model('Read_Model');
	/*get the menu id from menu name */
     $where = array('name' => $menuname);         
     $postid= $CI->Read_Model->dataread('gs_posts','id',$where)[0];
     /*ends with menuid code*/

     /*get menu data*/
     $mwhere = array('post_id'=>$postid['id'],'parent' => 0);
     $data2 =   $CI->Read_Model->dataread('gs_menu','',$mwhere,'menu_order','asc');
      /*get menu data*/

     if($data2){
         /*call menu html*/
         $MenuHtml = MenuHtmlData($data2,$get_parent_class,$childclass,$parentli,$innerul);
     }
     echo $MenuHtml;
}

function MenuHtmlData($data,$get_parent_class,$childclass,$parentli,$innerul){
    $CI = get_instance();
    $CI->load->model('Read_Model');
    $pclass = '';  // for main ul class
    $cClass = '';  // for li calss
    $child_class = ''; // for that li which contain dropdown ul class
    $get_parent_class1 = ''; // li parent ul class. classes for innerulonly
    $childclass1 = array('');
    if(! empty($get_parent_class)){
    	$pclass =  implode(' ', $get_parent_class);
    } 
    if(!empty($childclass)){
    	 $cClass =  implode(' ', $childclass);
    }
    $html = "";
    $html .= "  <ul class='".$pclass."'>";
    foreach($data  as  $value){        
        if($value['is_visible']=='1'){ 
			/* check li childs*/
			 $childs =  $CI->Read_Model->dataread('gs_menu','','post_id='.$value['post_id']. ' and parent= '.$value['id'] ,'menu_order','asc');
			 if($childs){
			 	if(!empty($parentli)){
			 			$child_class =  implode(' ', $parentli);
			 	}			 
			 }
			/*check li childs*/
        	$html .="<li class='".$child_class .' '.$cClass. "' data-id='".$value['id']."'>
        	<a href=".base_url($value['slug'])." > <span>	".$value['name']." </span></a>";
            $child_class = ''; //unset variable after asss append
           if($childs){           	
              $get_parent_class = $innerul; //change the parent of ul for inner ul
           	  $html .= MenuHtmlData($childs,$get_parent_class,$childclass,$parentli,$innerul);// recall function if find child of current menu id $Value["id"]
            }
            $html .="</li>";
                 $class = 'disable';
        }
    }
    $html .= "</ul>";   
    return $html;
       
}