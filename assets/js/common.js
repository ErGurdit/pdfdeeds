function alert_message(obj, table = '', formid = '') {
    var msg = obj.msg;
    if (obj.status == 'error' || obj.status == 'Error') {
        toastr.error(msg);
    } else {
        if (table) {
            table.ajax.reload();
        }
        if (formid) {
            $(formid)[0].reset();
        }
        toastr.success(msg);
    }
}

function alert_input_message($inputs, fromid = 'document') {
    $(fromid).find('.help-block').html(''); // make sure all previous message removed before nexr request
    $.each($inputs, function(key, value) {
        $(fromid).find('input[name="' + key + '"]').parent().find('.help-block').html(value);
        toastr.error(value);
    });
}