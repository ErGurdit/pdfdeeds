$(document).ready(function() {

    user_register_form();
    user_login_form();
    
});


function user_register_form() {
    $('#registerform').submit(function(e) {
        e.preventDefault();
        var formdata = $(this).serialize();
        $.post(baseurl + 'account/add', formdata, function(response) {
            var obj = JSON.parse(response);
            alert_message(obj, '', '#registerform');
            alert_input_message(obj.inputs, '#registerform');
        })

    })

}

function user_login_form() {

    $('#loginform').submit(function(e) {
        e.preventDefault();
        var formdata = $(this).serialize();
        $.post(baseurl + 'account/login', formdata, function(response) {
            var obj = JSON.parse(response);
            alert_message(obj, '', '#loginform');
            alert_input_message(obj.inputs, '#loginform');
            if(obj.status != 'error' || obj.status != 'Error'){
                 setTimeout(function(){  window.location.href = baseurl; }, 1000);
            }
           if(obj.redirect){
                window.location.href = obj.redirect; 
           }
           
        })

    })

}

function loadmore(){
    $('.loadmorebtn i').addClass('fa-li');
  $.post(baseurl + 'books/loadmore', 
        {
          offset :$('#offset').val(),
          limit :$('#limit').val(),
          urlid  : $('#urlid').val()
        }
    , function(data) {      
        setTimeout(function(){ $('.loadmorebtn i').removeClass('fa-li'); }, 2000);
        if(data){
            $('#load-more').append(data);
            $('.showing').html(parseInt($('#offset').val())+parseInt($('#limit').val()));
            $('#offset').val(parseInt($('#offset').val())+1);
        }else{
             $('.loadmorebtn').remove();
        }       

  });
}
