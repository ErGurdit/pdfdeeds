function alert_message(obj, table = '', formid = '') {
    var msg = obj.msg;
    if (obj.status == 'error') {
        toastr.error(msg);
    } else {
        if (table) {
            table.ajax.reload();
        }
        if (formid) {
            $(formid)[0].reset();
        }
        toastr.success(msg);
    }
}
